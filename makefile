start:
	docker-compose up

backend-test:
	python3 backend/tests.py

frontend-test:
	cd frontend && npm install && CI=true npm test

gui-test:
	python frontend/guitests.py

stop:
	docker-compose down

rebuild:
	docker-compose build
	docker-compose up

restart:
	make stop
	docker-compose up
