import ReginaImg from '../images/regina-chen.jpg';
import YujieImg from '../images/yujie-chen.jpg';
import MertImg from '../images/mert-hizli.png';
import PaulinaImg from '../images/paulina-lee.jpg';
import AadityaImg from '../images/aaditya-murthy.jpg';
import SarahImg from '../images/sarah-robertson.jpg';

export default function getDeveloperInfo(name) {

	const regina = {
		"name": "Regina Chen",
		"author_id": 2792868,
		"image": ReginaImg,
		"bio": "Current junior studying computer science at UT Austin. Responsible for front-end. Constantly craving Cane's.",
		"unit_tests": 3
	}

	const yujie = {
		"name": "Yujie Chen",
		"author_id": 2759818,
		"image": YujieImg,
		"bio": "Goes by Jessie and always covered in cat fur. Loves to eat and sleep, and maybe some coding too.",
		"unit_tests": 3
	}

	const mert = {
		"name": "Mert Hizli",
		"author_id": 2788605,
		"image": MertImg,
		"bio": "Responsible for front-end, a little bit backend, scrum methodologies.",
		"unit_tests": 6
	}

	const paulina = {
		"name": "Paulina Lee",
		"author_id": 2754807,
		"image": PaulinaImg,
		"bio": "Current junior studying computer science at UT Austin. Responsible for deployment and backend. Really likes cats.",
		"unit_tests": 4
	}

	const aaditya = {
		"name": "Aaditya Murthy",
		"author_id": 2765338,
		"image": AadityaImg,
		"bio": "Current Junior studying Computer Science and Mathematics at UT Austin. Love volleyball, puzzles, and music.",
		"unit_tests": 10
	}

	const sarah = {
		"name": "Sarah Robertson",
		"author_id": 2826911,
		"image": SarahImg,
		"bio": "Junior CS major at UT Austin. Into gamedev and dogs.",
		"unit_tests": 83
	}

	switch(name) {
		case 'Regina Chen':
			return regina;

		case 'Yujie Chen':
			return yujie;
		case 'Jessie Chen':
			return yujie;

		case 'Mert Hizli':
			return mert;
		case 'merthizli':
			return mert;

		case 'Paulina Lee':
			return paulina;

		case 'Aaditya Murthy':
			return aaditya;

		case 'Sarah Robertson':
			return sarah;
		case 'Sarah':
			return sarah;

		default:
			return 'not a member';
	}
}

export const tools_used = [
	{
		name: "React.js",
		usage: "JavaScript library for building user interface components"
	},
	{
		name: "D3.js",
		usage: "Javascript library for building interactive data visualizations"
	},
	{
		name: "Bootstrap",
		usage: "CSS framework used for its front-end component library"
	},
	{
		name: "Flask",
		usage: "Python microframework for building web applications"
	},
	{
		name: "Flask-RESTful",
		usage: "Python Flask extension for building REST APIs"
	},
	{
		name: "Flask-SQLAlchemy",
		usage: "Python Flask extension for database access and operations"
	},
	{
		name: "Axios",
		usage: "Promise based HTTP client for wrapping requests"
	},
	{
		name: "React Router",
		usage: "Routing library for navigation in React.js applications"
	},
	{
		name: "Beautiful Soup",
		usage: "Python library for automatic parsing and efficient scraping"
	},
	{
		name: "Selenium",
		usage: "GUI testing framework that uses browser automation"
	},
	{
		name: "Jest",
		usage: "JavaScript unit testing framework"
	},
	{
		name: "unittest",
		usage: "Python unit testing framework"
	},
	{
		name: "Postman",
		usage: "API design, development, and testing platform"
	},
	{
		name: "Docker",
		usage: "Platform for building and deploying applications with containers"
	},
	{
		name: "Amazon Web Services (AWS) & Elastic Compute Cloud (EC2)",
		usage: "Cloud computing services platform for application hosting"
	},
	{
		name: "GitLab",
		usage: "Version control system and continuous integration pipeline"
	},
	{
		name: "Slack",
		usage: "Team communication platform and integration with GitLab"
	}
]

export const data_sources = [
	{
		name: "Ballotpedia",
		url: "https://ballotpedia.org/Main_Page"
	},
	{
		name: "Vote.org",
		url: "https://www.vote.org/polling-place-locator/"
	},
	{
		name: "Google Civic Information API",
		url: "https://developers.google.com/civic-information/"
	}
]
