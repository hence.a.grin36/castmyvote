import React, { Component } from 'react';
import Axios from 'axios';

class Issues extends Component {
  constructor() {
    super();
    this.state = {
      count: 0
    }
  }

  componentWillMount() {
    const url = "https://gitlab.com/api/v4/projects/8576112/issues?author_id="

    Axios.get(url.concat(this.props.author_id))
      .then(response => {
        return response.data.length;
      })
      .then(data => {
        this.setState({count: data});
      })
  }

  render() {
    return (
      <span>
        {this.state.count}
      </span>
    );
  }
}

export default Issues;
