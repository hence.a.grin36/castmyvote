import React, { Component } from 'react';
import './Pagination.css';

function arraysEqual(arr1, arr2) {
    if(arr1.length !== arr2.length)
        return false;
    for(var i = arr1.length; i--;) {
        if(arr1[i] !== arr2[i])
            return false;
    }
    return true;
}
 
class Pagination extends Component {
    constructor(props) {
        super(props);
        this.state = { 
          pager: {},
          did_update: false
        };
    }
 
    componentWillMount() {
        // set page if items array isn't empty
        if (this.props.items && this.props.items.length) {
            this.setPage(this.props.current_page);
        }
    }
 
    componentDidUpdate(prevProps, prevState) {
      // reset page if items array has changed
      if (!arraysEqual(this.props.items, prevProps.items) && !this.state.did_update) {
          this.setPage(this.props.initialPage);
      }
    }
 
    setPage(page) {
      var changepage = true;
      //check to eliminate a console error that otherwise does not affect code
      if (page === undefined) {
        changepage = false;
      }
      var items = this.props.items;
      var pager = this.state.pager;

      if (page < 1 || page > pager.totalPages) {
          return;
      }

      // get new pager object for specified page
      pager = this.getPager(items.length, page);

      // update state
      this.setState({ pager: pager });

      if (changepage) {
        // call change page function in parent component
        this.props.onChangePage(this.props.url + "?page=" + page);
      }
    }

    getPager(totalItems, currentPage) {
        // default to first page
        currentPage = this.props.current_page;
 
        // calculate total pages
        var totalPages = this.props.total_pages;
        var previousPage = this.props.prev_page;
        var nextPage = this.props.next_page;
 
        var startPage, endPage;
        if (totalPages <= 10) {
            // less than 10 total pages so show all
            startPage = 1;
            endPage = totalPages;
        } else {
            // more than 10 total pages so calculate start and end pages
            if (currentPage <= 6) {
                startPage = 1;
                endPage = 10;
            } else if (currentPage + 4 >= totalPages) {
                startPage = totalPages - 9;
                endPage = totalPages;
            } else {
                startPage = currentPage - 5;
                endPage = currentPage + 4;
            }
        }

        // create an array of pages to ng-repeat in the pager control
        var pages = [...Array((endPage + 1) - startPage).keys()].map(i => startPage + i);
 
        // return object with all pager properties required by the view
        return {
            currentPage: currentPage,
            previousPage: previousPage,
            nextPage: nextPage,
            totalPages: totalPages,
            startPage: startPage,
            endPage: endPage,
            pages: pages
        };
    }


    render() {
        var pager = this.state.pager;
 
        if (!pager.pages || pager.pages.length <= 1) {
            // don't display pager if there is only 1 page
            return null;
        }
        return (
            <ul className="pagination">
                <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                    <a onClick={() => this.setPage(1)}>first</a>
                </li>
                <li className={pager.currentPage === 1 ? 'disabled' : ''}>
                    <a onClick={() => this.setPage(pager.previousPage)}>prev</a>
                </li>
                {pager.pages.map((page, index) =>
                    <li key={index} className={pager.currentPage === page ? 'active' : ''}>
                        <a onClick={() => this.setPage(page)}>{page}</a>
                    </li>
                )}
                <li className={pager.currentPage === pager.totalPages ? 'disabled' : ''}>
                    <a onClick={() => this.setPage(pager.nextPage)}>next</a>
                </li>
                <li className={pager.currentPage === pager.totalPages ? 'disabled' : ''}>
                    <a onClick={() => this.setPage(pager.totalPages)}>last</a>
                </li>
            </ul>
        );
    }
}

export default Pagination;
