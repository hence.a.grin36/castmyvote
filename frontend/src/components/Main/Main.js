import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Home from '../Home/Home';
import Elections from '../Elections/Elections';
import Election from '../Election/Election';
import Candidates from '../Candidates/Candidates';
import Candidate from '../Candidate/Candidate';
import Locations from '../Locations/Locations';
import Location from '../Location/Location';
import About from '../About/About';
import Visualizations from '../Visualization/Visualizations';
import SearchAll from '../SearchAll/SearchAll';
import UsaMapVis from '../Visualization/UsaMapVis';
import Bubble from '../Visualization/Bubble/Bubble';
import Bubble2 from '../Visualization/Bubble/Bubble2';
import Bubble3 from '../Visualization/Bubble/Bubble3';
import Bubble4 from '../Visualization/Bubble/Bubble4';
import UsaMapVis2 from '../Visualization/UsaMapVis2';
import PovertyPie from '../Visualization/PovertyPie'
import './Main.css';

const Main = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/elections/:id' component={Election}/>
      <Route path='/elections' component={Elections}/>

      <Route path='/candidates/:id' component={Candidate}/>
      <Route path='/candidates/' component={Candidates}/>

      <Route path='/locations/:id' component={Location}/>
      <Route path='/locations' component={Locations}/>
      <Route path='/about' component={About}/>
      <Route path='/search' component={SearchAll}/>

      <Route path='/visualizations' component={Visualizations}/>
      <Route path='/map' component={UsaMapVis}/>
      <Route path='/bubble' component={Bubble}/>
      <Route path='/bubble2' component={Bubble2}/>
      <Route path='/bubble3' component={Bubble3}/>
      <Route path='/bubble4' component={Bubble4}/>
      <Route path='/map2' component={UsaMapVis2}/>
      <Route path='/povertypie' component={PovertyPie}/>
    </Switch>
  </main>
)

export default Main
