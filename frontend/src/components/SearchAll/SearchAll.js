import React, { Component } from 'react';
import Search from '../Search/Search';
import ElectionsGrid from '../Elections/ElectionsGrid';
import CandidatesGrid from '../Candidates/CandidatesGrid';
import LocationsGrid from '../Locations/LocationsGrid';
import './SearchAll.css';

class SearchAll extends Component {
  constructor () {
    super();
    this.search = this.search.bind(this);
  }

  search(searchText) {
    this.electionsGrid.search(searchText);
    this.candidatesGrid.search(searchText);
    this.locationsGrid.search(searchText);
  }

  render() {
    return (
      <div className="SearchAll">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <h1 className="section-header">Search</h1>
            </div>
            <div className="col-md-4">
              <Search search_item="anything" search={this.search}/>
            </div>
          </div>
        </div>
        <h4 className="grid-header">Elections</h4>
        <ElectionsGrid ref={electionsGrid => this.electionsGrid = electionsGrid}/>
        <hr/>
        <h4 className="grid-header">Candidates</h4>
        <CandidatesGrid ref={candidatesGrid => this.candidatesGrid = candidatesGrid}/>
        <hr/>
        <h4 className="grid-header">Locations</h4>
        <LocationsGrid ref={locationsGrid => this.locationsGrid = locationsGrid}/>
      </div>
    )
  }
}

export default SearchAll;
