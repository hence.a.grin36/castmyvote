import React, { Component } from 'react';
import Search from '../Search/Search';
import ElectionsGrid from './ElectionsGrid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUndo } from '@fortawesome/free-solid-svg-icons';
import './Elections.css';

class Elections extends Component {
  constructor () {
    super();
    this.sort = this.sort.bind(this);
    this.sort_desc = this.sort_desc.bind(this);
    this.filter = this.filter.bind(this);
    this.search = this.search.bind(this);
    this.reset = this.reset.bind(this);
  }

  sort(e) {
    this.electionsGrid.sort(e);
  }

  sort_desc(e) {
    this.electionsGrid.sort_desc(e);
  }

  filter(e) {
    this.electionsGrid.filter(e);
  }

  search(searchText) {
    this.electionsGrid.search(searchText);
  }

  reset() {
    this.electionsGrid.reset();
  }

  render() {
    return (
      <div className="Elections">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <h1 className="section-header">Elections</h1>
            </div>
            <div className="col-md-4">
              <Search search_item="elections" search={this.search}/>
            </div>
          </div>
          <div className="row d-flex justify-content-end">
            <div className="dropdown">
              <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort By</button>
              <div className="dropdown-menu">
                <button className="dropdown-item" type="button" value="name" onClick={this.sort}>Name A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="name" onClick={this.sort_desc}>Name Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="date" onClick={this.sort}>Date A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="date" onClick={this.sort_desc}>Date Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="election_type" onClick={this.sort}>Election Type A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="election_type" onClick={this.sort_desc}>Election Type Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="office_name" onClick={this.sort}>Office Name A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="office_name" onClick={this.sort_desc}>Office Name Z-A</button>
              </div>
            </div>
            <div className="dropdown">
              <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter By</button>
              <div className="dropdown-menu">
                <button className="dropdown-item" type="button" value="tx_house" onClick={this.filter}>Texas House of Representatives</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="tx_senate" onClick={this.filter}>Texas Senate</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="us_house" onClick={this.filter}>U.S. House of Representatives</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="us_senate" onClick={this.filter}>U.S. Senate</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="texas" onClick={this.filter}>Texas Statewide</button>
              </div>
            </div>
            <button className="btn btn-primary reset" onClick={this.reset}>
              <FontAwesomeIcon icon={faUndo}/>
            </button>
          </div>
        </div>
        <ElectionsGrid ref={electionsGrid => this.electionsGrid = electionsGrid}/>
      </div>
    )
  }
}

export default Elections;
