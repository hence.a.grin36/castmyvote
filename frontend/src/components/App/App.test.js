import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {shallow} from 'enzyme';

configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
  	<BrowserRouter>
  		<App />
  	</BrowserRouter>
  , div);
  ReactDOM.unmountComponentAtNode(div);
});

// it('check that the navbar is displayed', () => {
//   const Ap = shallow(<App/>);
//   expect(Ap.contains('Navbar')).to.equal(true);
// });

// it('check that main is displayed', () => {
//   const Ap = shallow(<App/>);
//   expect(Ap.contains('Main')).to.equal(true);
// });