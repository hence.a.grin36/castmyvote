import React, { Component } from 'react';
import Search from '../Search/Search';
import LocationsGrid from './LocationsGrid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUndo } from '@fortawesome/free-solid-svg-icons';
import './Locations.css';

class Locations extends Component {
  constructor () {
    super();
    this.sort = this.sort.bind(this);
    this.sort_desc = this.sort_desc.bind(this);
    this.filter = this.filter.bind(this);
    this.search = this.search.bind(this);
    this.reset = this.reset.bind(this);
  }

  sort(e) {
    this.locationsGrid.sort(e);
  }

  sort_desc(e) {
    this.locationsGrid.sort_desc(e);
  }

  filter(e) {
    this.locationsGrid.filter(e);
  }

  search(searchText) {
    this.locationsGrid.search(searchText);
  }

  reset() {
    this.locationsGrid.reset()
  }

  render() {
    return (
      <div className="Locations">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <h1 className="section-header">Locations</h1>
            </div>
            <div className="col-md-4">
              <Search search_item="locations" search={this.search}/>
            </div>
          </div>
          <div className="row d-flex justify-content-end">
            <div className="dropdown">
              <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort By</button>
              <div className="dropdown-menu">
                <button className="dropdown-item" type="button" value="name" onClick={this.sort}>Name A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="name" onClick={this.sort_desc}>Name Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="population" onClick={this.sort}>Population A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="population" onClick={this.sort_desc}>Population Z-A</button>
              </div>
            </div>
            <div className="dropdown">
              <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter By</button>
              <div className="dropdown-menu">
                <button className="dropdown-item" type="button" value="100000" onClick={this.filter}>Population Greater Than 100,000</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="200000" onClick={this.filter}>Population Greater Than 200,000</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="500000" onClick={this.filter}>Population Greater Than 500,000</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="800000" onClick={this.filter}>Population Greater Than 800,000</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="1000000" onClick={this.filter}>Population Greater Than 1,000,000</button>
              </div>
            </div>
            <button className="btn btn-primary reset" onClick={this.reset}>
              <FontAwesomeIcon icon={faUndo}/>
            </button>
          </div>
        </div>
        <LocationsGrid ref={locationsGrid => this.locationsGrid = locationsGrid}/>
      </div>
    )
  }
}

export default Locations;
