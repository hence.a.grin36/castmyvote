import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import Highlighter from "react-highlight-words";
import Pagination from '../Pagination/Pagination';
import './Locations.css';

const URL = "http://api.castmyvote.me/locations"
// const URL = "http://localhost:5000/locations"

class LocationsGrid extends Component {
  constructor () {
    super();
    this.state = {
      locations:[],
      total_pages: 0,
      current_page: 1,
      next_page: "",
      previous_page: "",
      searchURL: "",
      searchText: "",
      sortURL: "",
      filterURL: ""
    };
    this.onChangePage = this.onChangePage.bind(this);
    this.sort = this.sort.bind(this);
    this.filter = this.filter.bind(this);
  }

  componentDidMount() {
    this.reset();
  }

  onChangePage(url) {
    if (this.state.sortURL !== "") {
      url = url.concat(`&${this.state.sortURL}`);
    }
    if (this.state.filterURL !== "") {
      url = url.concat(`&${this.state.filterURL}`);
    }
    if (this.state.searchURL !== "") {
      url = url.concat(`&${this.state.searchURL}`);
    }
    Axios.get(url)
      .then(response => response.data)
      .then(data => this.setState({
        locations: data.results,
        current_page: data.current_page,
        next_page: data.next,
        previous_page: data.previous,
    }));
  }

  formatPopulation(population) {
    if (population) {
      return population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }

  sort(e) {
    var newSort = e.target.value;
    var sortURL = `sort=${newSort}`;
    var newURL = URL.concat(`?${sortURL}`);
    if (this.state.searchURL) {
      newURL = newURL.concat(`&${this.state.searchURL}`);
    }
    if (this.state.filterURL) {
      newURL = newURL.concat(`&${this.state.filterURL}`);
    }
    Axios.get(newURL)
      .then(response => response.data)
      .then(data => this.setState({
        locations: data.results,
        total_pages: data.total_pages,
        current_page: data.current_page,
        next_page: data.next,
        previous_page: data.previous,
        sortURL: sortURL
      }));
  }

  sort_desc(e) {
    var newSort = e.target.value;
    var newOrder = "desc";
    var sortURL = `sort=${newSort}&order=${newOrder}`;
    var newURL = URL.concat(`?${sortURL}`);
    Axios.get(newURL)
      .then(response => response.data)
      .then(data => this.setState({
        locations: data.results,
        total_pages: data.total_pages,
        current_page: data.current_page,
        next_page: data.next,
        previous_page: data.previous,
        sortURL: sortURL
      }));
  }

  filter(e) {
    var newFilter = e.target.value;
    var filterURL = `population=${newFilter}`;
    var newURL = URL.concat(`?${filterURL}`);
    if (this.state.searchURL) {
      newURL = newURL.concat(`&${this.state.searchURL}`);
    }
    if (this.state.sortURL) {
      newURL = newURL.concat(`&${this.state.sortURL}`);
    }
    Axios.get(newURL)
      .then(response => response.data)
      .then(data => this.setState({
        locations: data.results,
        total_pages: data.total_pages,
        current_page: data.current_page,
        next_page: data.next,
        previous_page: data.previous,
        filterURL: filterURL
      }));
  }

  search(searchText) {
    this.setState({searchText: searchText.toLowerCase()})
    const searchURL = `search=${searchText}`;
    let newURL = URL.concat(`?${searchURL}`)
    if (searchText !== "") {
      if (this.state.sortURL) {
        newURL = newURL.concat(`&${this.state.sortURL}`);
      }
      if (this.state.filterURL) {
        newURL = newURL.concat(`&${this.state.filterURL}`);
      }
    } else {
      this.setState({
        sortURL: "",
        filterURL: ""
      })
    }
    Axios.get(newURL)
      .then(response => response.data)
      .then(data => this.setState({
        locations: data.results,
        total_pages: data.total_pages,
        current_page: data.current_page,
        next_page: data.next,
        previous_page: data.previous,
        searchURL: searchURL
      }));
  }

  reset() {
    Axios.get(URL)
      .then(response => response.data)
      .then(data => this.setState({ 
        locations: data.results, 
        total_pages: data.total_pages, 
        current_page: data.current_page, 
        next_page: data.next, 
        previous_page: data.previous,
        sortURL: "",
        filterURL: "",
        searchURL: "",
        searchText: ""
      }));
  }

  renderInfo(key, value) {
    return (
      <div className="mb-1">
        <strong>{key + ":"}</strong> {value ? <Highlighter
          highlightClassName="highlight"
          searchWords={[this.state.searchText]}
          autoEscape={true}
          textToHighlight={value.toString()}
        /> : <i>none</i>}
      </div>
    )
  }

  renderCard(location) {
    return (
      <div className="col-md-3" key={location.id}>
        <Link to={'/locations/' + location.id} className="card grid-instance">
          <div className="card-body">
            <h5 className="card-title">
              <Highlighter
                highlightClassName="highlight"
                searchWords={[this.state.searchText]}
                autoEscape={true}
                textToHighlight={location.name}
              />
            </h5>
            <h6 className="card-subtitle mb-2 text-muted">
              Population: <Highlighter
                highlightClassName="highlight"
                searchWords={[this.state.searchText]}
                autoEscape={true}
                textToHighlight={this.formatPopulation(location.population)}
              />
            </h6>
            <div className="card-info">
              {this.renderInfo("Incumbent", location.current_incumbent)}
              {this.renderInfo("Voting Age", location.voting_age && location.voting_age.substring(0, location.voting_age.indexOf('%') + 1))}
              {this.renderInfo("Elections", location.elections.map(election => election.name.split("- ")[1]).join(', '))}
            </div>
          </div>
        </Link>
      </div>
    )
  }

  renderGrid(locations) {
    const rowLength = 4;
    return locations.map(location => {
      return this.renderCard(location);
    }).reduce((r, element, index) => {
      index % rowLength === 0 && r.push([]);
      r[r.length - 1].push(element);
      return r;
    }, []).map((rowContent, i) => {
      return <div className="row" key={i}>{rowContent}</div>;
    })
  }

  render() {
    return (
      <div className="Locations">
        <div className="container">
          {this.state.locations && this.renderGrid(this.state.locations)}
        </div>
        <div className="row justify-content-center">
          <Pagination items={this.state.locations}
          onChangePage={this.onChangePage}
          current_page={this.state.current_page}
          next_page={this.state.current_page + 1}
          prev_page={this.state.current_page - 1}
          url={URL}
          total_pages={this.state.total_pages} />
        </div>
      </div>
    )
  }
}

export default LocationsGrid;
