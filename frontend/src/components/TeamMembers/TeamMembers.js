import React, { Component } from 'react';
import Axios from 'axios';
import Issues from '../Issues/Issues';
import getDeveloperInfo from '../../assets/static/Static';
import './TeamMembers.css';

class TeamMembers extends Component {
  constructor() {
    super();
    this.state = {
      developers: []
    }
  }

  componentWillMount() {
    const url = "https://gitlab.com/api/v4/projects/8576112/repository/contributors?sort=desc";

    Axios.get(url)
      .then(response => {
        const data = response.data;
        let developers = [];
        let indices = {};
        let indicesIndex = 0;

        for (var i = 0; i < data.length; i++) {
          const developer = data[i];
          const developerInfo = getDeveloperInfo(developer.name);

          if (developerInfo === 'not a member') {
            continue;
          }

          developer.name = developerInfo.name;
          developer.author_id = developerInfo.author_id;
          developer.image = developerInfo.image;
          developer.bio = developerInfo.bio;
          developer.unit_tests = developerInfo.unit_tests;

          if (Object.keys(indices).includes(developer.name)) {
            let index = indices[developer.name];
            developers[index].commits += developer.commits;
          } else {
            developers.push(developer);
            indices[developer.name] = indicesIndex++;
          }
        }
        return developers;
      })
    .then(data => {
      this.setState({developers: data})
    });
  }

  getTotalCommits() {
    let sum = 0;
    for (var i = 0; i < this.state.developers.length; i++) {
      sum += this.state.developers[i].commits;
    }
    return sum;
  }

  renderHelper(start, end) {
    const items = this.state.developers.slice(start, end);

    return (
      <div className="row">
      {
        items.map((developer, i) => {
          return (
            <div className="col-md-4" key={start + i}>
              <div className="profile">
                <img className="profile-img" src={developer.image} alt={developer.name} />
                <div className="profile-content">
                  <div className="profile-name">
                    <h6>
                      {developer.name}
                    </h6>
                    <small className="text-muted">
                      developer
                    </small>
                  </div>
                  <div className="profile-description text-muted">
                    {developer.bio}
                  </div>
                </div>
                <div className="profile-stats text-muted">
                  <p className="text-muted">
                    Issues: <Issues author_id={developer.author_id} />
                    <br />
                    Commits: {developer.commits}
                    <br />
                    Unit Tests: {developer.unit_tests}
                  </p>
                </div>
              </div>
            </div>
          )
        })
      }
      </div>
    )
  }

  render() {
    return (
      <div className="TeamMembers">
        {this.renderHelper(0, 3)}
        {this.renderHelper(3, 6)}
      </div>
    );
  }
}

export default TeamMembers;
