import React, { Component } from 'react';
import './Search.css';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: ''
    };
  }

  onInputChange(searchText) {
    this.setState({searchText});
  }

  render() {
    return (
      <div className="Search">
        <form onSubmit={(e) => {e.preventDefault(); this.props.search(this.state.searchText)}}>
          <div className="form-group">
            <input
              type="text"
              className="form-control search-bar"
              id={this.props.search_item + "Search"}
              placeholder={"search for " + this.props.search_item + "..."}
              value={this.state.searchText}
              onChange={event => this.onInputChange(event.target.value)} 
            />
          </div>
        </form>
      </div>
    );
  }
}

export default Search;
