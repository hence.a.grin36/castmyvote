import React from 'react';
import ReactDOM from 'react-dom';
import Search from './Search';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {shallow} from 'enzyme';

configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
  	<Search />
  , div);
  ReactDOM.unmountComponentAtNode(div);
});

it('enter text changes searchText', () => {
  const searchBar = shallow(<Search/>);
  searchBar.find('input').simulate('change', {'target': {'value': 'Texas'}});
  expect(searchBar.state().searchText).toEqual('Texas');
});