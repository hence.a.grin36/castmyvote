import React, { Component } from 'react';
import Axios from 'axios';
import TeamMembers from '../TeamMembers/TeamMembers';
import { tools_used, data_sources } from '../../assets/static/Static';
import './About.css';

class About extends Component {
  constructor() {
    super();
    this.state = {
      total_commits: 0,
      total_issues: 0
    }
  }

  componentWillMount() {
    const issuesURL = "https://gitlab.com/api/v4/projects/8576112/issues?per_page=100";
    Axios.get(issuesURL)
      .then(response => {
        return response.data.length;
      })
      .then(data => {
        this.setState({total_issues: data});
      })
  }

  render() {
    const repoURL = "https://gitlab.com/hence.a.grin36/castmyvote";
    const postmanURL = "https://documenter.getpostman.com/view/5462463/RWgm3ggs"

    return (
      <div className="About">
      	<div className="container-fluid">
          <div className="container">
            <div className="section-header">
              The Project
            </div>
            <div className="project-details">
              <p>
                We aim to inspire higher voter turnout by providing information about local elections to potential and registered voters in Texas.
                Since local elections are generally less publicized but have greater impact on a person's day-to-day life, 
                we strive to raise awareness about local elections, inform voters about local representatives, and encourage invididuals to vote in upcoming elections.
              </p>
              <a href={repoURL} target="_blank" rel="noopener noreferrer">
                GitLab Repo
              </a>
              <br />
              <a href={postmanURL} target="_blank" rel="noopener noreferrer">
                Postman API
              </a>
            </div>
          </div>
          <hr />

          <div className="container">
            <div className="section-header">
              The Team
            </div>
            <TeamMembers ref={teamMembers => this.teamMembers = teamMembers}/>
          </div>
          <hr />
          <div className="container">
            <div className="section-header">
              The Stats
            </div>
            <div>
              <ul className="list-group">
                <li className="list-group-item d-flex justify-content-between align-items-center">
                  Total commits
                  <span className="badge badge-primary badge-pill">{this.teamMembers && this.teamMembers.getTotalCommits()}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center">
                  Total issues
                  <span className="badge badge-primary badge-pill">{this.state.total_issues}</span>
                </li>
                <li className="list-group-item d-flex justify-content-between align-items-center">
                  Total unit tests
                  <span className="badge badge-primary badge-pill">109</span>
                </li>
              </ul>
            </div>
          </div>
          <hr />
          <div className="container">
            <div className="section-header">
              The Data
            </div>
            <div className="project-details">
              <p>
                Election, candidate, and location data was programatically scraped from Ballotpedia using Beautiful Soup,
                a Python library for efficient parsing and scraping. Further information about location was also 
                systematically retrived from the Google Civic Information API. The data was supplemented with images 
                scraped from Bing and Yahoo.
              </p>
              <p>
                By integrating different source's disparate data, we discovered the multitide of relationships between 
                data attributes. We were able to analyze these relationships to produce new, meaningful information for 
                a more comprehensive database of local Texas elections.
              </p>
            </div>
            <div>
              <ul className="list-group">
              {
                data_sources.map((source, i) => (
                  <li className="list-group-item" key={i}>
                    <a href={source.url}>
                      {source.name}
                    </a>
                  </li>
                ))
              }
              </ul>
            </div>
          </div>
          <hr />
          <div className="container">
            <div className="section-header">
              The Tools
            </div>
            <div>
              <ul className="list-group">
              {
                tools_used.map((tool, i) => (
                  <li className="list-group-item" key={i}>
                    {tool.name}
                    <br />
                    <span className="text-muted">{tool.usage}</span>
                  </li>
                ))
              }
              </ul>
            </div>
          </div>
      	</div>
      </div>
    );
  }
}

export default About;
