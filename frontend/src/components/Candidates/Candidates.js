import React, { Component } from 'react';
import Search from '../Search/Search';
import CandidatesGrid from './CandidatesGrid';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUndo } from '@fortawesome/free-solid-svg-icons';
import './Candidates.css';

class Candidates extends Component {
  constructor () {
    super();
    this.sort = this.sort.bind(this);
    this.sort_desc = this.sort_desc.bind(this);
    this.filter = this.filter.bind(this);
    this.search = this.search.bind(this);
    this.reset = this.reset.bind(this);
  }

  sort(e) {
    this.candidatesGrid.sort(e)
  }

  sort_desc(e) {
    this.candidatesGrid.sort_desc(e)
  }

  filter(e) {
    this.candidatesGrid.filter(e)
  }

  search(searchText) {
    this.candidatesGrid.search(searchText)
  }

  reset() {
    this.candidatesGrid.reset()
  }

  render() {
    return (
      <div className="Candidates">
        <div className="container">
          <div className="row">
            <div className="col-md-8">
              <h1 className="section-header">Candidates</h1>
            </div>
            <div className="col-md-4">
              <Search search_item="candidates" search={this.search}/>
            </div>
          </div>
          <div className="row d-flex justify-content-end">
            <div className="dropdown">
              <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort By</button>
              <div className="dropdown-menu">
                <button className="dropdown-item" type="button" value="name" onClick={this.sort}>Name A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="name" onClick={this.sort_desc}>Name Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="party" onClick={this.sort}>Party A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="party" onClick={this.sort_desc}>Party Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="incumbent" onClick={this.sort}>Incumbent Status A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="incumbent" onClick={this.sort_desc}>Incumbent Status Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="running_office" onClick={this.sort}>Running Office A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="running_office" onClick={this.sort_desc}>Running Office Z-A</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="current_office" onClick={this.sort}>Current Office A-Z</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="current_office" onClick={this.sort_desc}>Current Office Z-A</button>
              </div>
            </div>
            <div className="dropdown">
              <button className="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter By</button>
              <div className="dropdown-menu">
                <button className="dropdown-item" type="button" value="true" onClick={this.filter}>Incumbents</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="false" onClick={this.filter}>Challengers</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="republican" onClick={this.filter}>Republicans</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="democrat" onClick={this.filter}>Democrats</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="independent" onClick={this.filter}>Independents</button>
                <div className="dropdown-divider"></div>
                <button className="dropdown-item" type="button" value="libertarian" onClick={this.filter}>Libertarians</button>
              </div>
            </div>
            <button className="btn btn-primary reset" onClick={this.reset}>
              <FontAwesomeIcon icon={faUndo}/>
            </button>
          </div>
        </div>
        <CandidatesGrid ref={candidatesGrid => this.candidatesGrid = candidatesGrid}/>
      </div>
    )
  }
}

export default Candidates;
