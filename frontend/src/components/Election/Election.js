import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import './Election.css';

class Election extends Component {
  constructor() {
    super();
    this.state = {
      election: {}
    }

    this.renderLink = this.renderLink.bind(this);
    this.renderInfo = this.renderInfo.bind(this);
  }

  componentWillMount() {
    const { id } = this.props.match.params

    let url = "http://api.castmyvote.me/elections/" + id;

    Axios.get(url)
      .then(response => response.data)
      .then(data => this.setState({election: data}))
  }

  findIncumbent(candidates) {
    if (candidates) {
      for (let i = 0; i < candidates.length; i++) {
        let candidate = candidates[i];
        if (candidate.incumbent) {
          return (
            <Link to={"/candidates/" + candidate.id}>
              {candidate.name} ({candidate.party.charAt(0)})
            </Link>
          )
        }
      }
    }
    return <i>empty</i>;
  }

  renderLink(value, url) {
    return value && value.map(val => {
      return <Link to={url}>{val}</Link>
    })
  }

  renderInfo(key, value) {
    return (
      <div className="election-info">
        <strong>{key + ":"}</strong><br/>{value ? value : <i>none</i>}
      </div>
    )
  }

  renderCandidates(candidates) {
    return candidates && candidates.map(candidate => {
      return (
        <div className="col m-2 card text-left candidate" key={candidate.id}>
          <Link to={"/candidates/" + candidate.id}>
            <div className="card-img-top text-center">
              <img src={candidate.image} alt={candidate.name}/>
            </div>
            <div className="card-body text-center">
              {candidate.name} ({candidate.party.charAt(0)})
            </div>
          </Link>
        </div>
      )
    })
  }

  renderLocation(location) {
    return location && (
      <Link to={"/locations/" + location.id}>
        {location.name}
      </Link>
    )
  }

  render() {
    const election = this.state.election;

    return (
      <div className="Election">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h6 className="text-muted text-left mb-0">Election</h6>
              <h1 className="section-header mb-5">{election.name}</h1>
              <div className="container">
                <div className="row">
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Date", election.date)}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Type", election.election_type)}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Office", election.office_name)}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Location", this.renderLocation(election.location))}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Incumbent", this.findIncumbent(election.candidates))}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Winner", election.winner ? election.winner : <i>to be decided</i>)}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      <div className="election-info text-center">
                        <strong>Candidates:</strong>
                      </div>
                      <div className="row d-flex justify-content-center">
                        {this.renderCandidates(election.candidates)}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Election;
