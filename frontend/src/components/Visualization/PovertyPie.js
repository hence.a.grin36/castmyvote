import React, { Component } from 'react';
import { PieChart, Legend } from 'react-easy-chart';


class PovertyPie extends Component {
  render() {
    return(
      <div id='pie'>
        <br></br>
        <div>Extreme Poverty in School Districts Nationally</div>
        <br></br>
        <PieChart
          size={500}
          padding={0}
          labels
          data={[
            { key: '38 (27.14%)', value: 38, color: '#eb6b6b' },
            { key: '102 (72.86%)', value: 102, color: '#88ca6d' }
          ]}
          styles={{
            '.chart_text': {
              fontSize: '20em',
              fill: '#fff'

            }
          }}
        /><br></br>
        <Legend
          data={[
            { key: 'Extreme', value: 38, color: '#eb6b6b' },
            { key: 'Non-Extreme', value: 102, color: '#88ca6d' }
          ]}
          dataId={'key'}
          config={[
            {color: '#eb6b6b'},
            {color: '#88ca6d'}
          ]}
          horizontal />
      </div>
    );
  }
}

export default PovertyPie;
