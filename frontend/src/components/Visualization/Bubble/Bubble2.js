import React, { Component } from 'react';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import './Chart.css';

class Bubble2 extends Component {
  render() {
    const graph = {
      zoom: .8,
      offsetX: 0,
      offsetY: 0,
    }

    return (
      <div className="Chart">
        <div className="container">
          <h1 className="title">Candidates and Parties</h1>
          <BubbleChart
            graph={graph}
            width={1200}
            height={800}
            fontFamily="Arial"
            data= {[
              { label: 'Libertarian', value: 76 },
              { label: 'Independent', value: 10 },
              { label: 'Republican', value: 179 },
              { label: 'Democrat', value: 199 }]
            }
          />
        </div>
      </div>
    );
  }
}

export default Bubble2;