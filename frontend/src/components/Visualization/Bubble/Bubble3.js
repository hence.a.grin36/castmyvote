import React, { Component } from 'react';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import './Chart.css';

class Bubble3 extends Component {
  render() {
    const graph = {
      zoom: .8,
      offsetX: 0,
      offsetY: 0,
    }

    return (
      <div className="Chart">
        <div className="container">
          <h1 className="title">Texas Demographics</h1>
          <BubbleChart
            graph={graph}
            width={1200}
            height={800}
            fontFamily="Arial"
            data= {[
              { label: 'White', value: 75 },
              { label: 'Black', value: 12 },
              { label: 'Asian', value: 4 },
              { label: 'Other', value: 9 }]
            }
          />
        </div>
      </div>
    );
  }
}

export default Bubble3;