import React, { Component } from 'react';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import './Chart.css';

class Bubble4 extends Component {
  render() {
    const graph = {
      zoom: .8,
      offsetX: 0,
      offsetY: 0,
    }

    return (
      <div className="Chart">
        <div className="container">
          <h1 className="title">Poverty School Districts per State</h1>
          <BubbleChart
            graph={graph}
            width={1200}
            height={800}
            fontFamily="Arial"
            data= {[
              {label:  'Mississippi' , value:  24 },
{label:  'Illinois' , value:  3 },
{label:  'New Mexico' , value:  7 },
{label:  'Alabama' , value:  9 },
{label:  'Texas' , value:  21 },
{label:  'Louisiana' , value:  6 },
{label:  'Kentucky' , value:  12 },
{label:  'Michigan' , value:  9 },
{label:  'Oregon' , value:  1 },
{label:  'Montana' , value:  1 },
{label:  'New York' , value:  1 },
{label:  'Arkansas' , value:  6 },
{label:  'Ohio' , value:  7 },
{label:  'Georgia' , value:  8 },
{label:  'South Dakota' , value:  3 },
{label:  'New Jersey' , value:  2 },
{label:  'South Carolina' , value:  3 },
{label:  'Arizona' , value:  6 },
{label:  'California' , value:  3 },
{label:  'Colorado' , value:  1 },
{label:  'Oklahoma' , value:  2 },
{label:  'Alaska' , value:  1 },
{label:  'Pennsylvania' , value:  1 },
{label:  'Missouri' , value:  1 },
{label:  'North Dakota' , value:  1 },
{label:  'Indiana' , value:  1 },]
            }
          />
        </div>
      </div>
    );
  }
}

export default Bubble4;