import React, { Component } from 'react';
import BubbleChart from '@weknow/react-bubble-chart-d3';
import './Chart.css';

class Bubble extends Component {
 constructor(props) {
    super(props);
    this.state= {
      locations: {},
      data1:[]
    }
  }
    
  componentWillMount() {
    fetch('http://api.castmyvote.me/bubble')
      .then((resp) => resp.json()) 
      .then(data => this.setState({locations: data}))
  }


  render() {
    for (let state in this.state.locations){
      var bookTitle = this.state.locations[state].name;
      var author = this.state.locations[state].population;
      var foo = {};
      foo['label'] = bookTitle;
      foo['value'] = parseFloat(author)*1000
      this.state.data1.push(foo)
    }

    const graph = {
      zoom: .8,
      offsetX: 0,
      offsetY: 0,
    }

    return (
      <div className="Chart">
        <div className="container">
          <h1 className="title">District Populations</h1>
          <BubbleChart
            graph={graph}
            width={1200}
            height={800}
            fontFamily="Arial"
            data= {this.state.data1}
          />
        </div>
      </div>
    );
  }
}

export default Bubble;