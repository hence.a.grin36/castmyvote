import React, {Component} from 'react';
import UsaMap from './UsaMap'

class UsaMapVis extends Component {
  constructor(props) {
    super(props);
    this.state= {
      texas: {},
    }
  }
  
  componentWillMount() {
    fetch('http://api.castmyvote.me/map')
      .then((resp) => resp.json()) 
      .then(data => this.setState({texas: data}))
  }

  render(){
    let image = {
        background: 'transparent',
    }

    return(
      <div>
        <div className="texas-map" >
          <div className="jumbotron p-3 p-md-5 rounded" style={image}>
            <div className ="jumbotron_wrapper container">
              <h1><span id="d3text">USA MAP</span></h1>
              <h6><span id="d3text">Click on Texas for more info</span></h6>
              <UsaMap info={this.state.texas}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default UsaMapVis;
