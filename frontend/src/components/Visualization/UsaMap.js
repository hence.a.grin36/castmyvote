import React, { Component } from 'react';
import {drawMap} from './States';
import * as d3 from 'd3';
import './UsaMap.css'

function tooltipHtml(table_name, attributes){ /* function to create html content string in tooltip div. */
  let table = "<h4>"+table_name+"</h4><table>"
  for (var key in attributes){
    if (key !== 'color')
      table = table.concat("<tr><td>", key, "</td><td>", attributes[key], "</td></tr>")
  }
  return table.concat("</table>");
}

export default class UsaMap extends Component {

  componentDidMount(){
    
    d3.select("div#mapd3")
      .append("svg")
      .attr("id", "statesvg")
      .attr("preserveAspectRatio", "xMinYMin meet")
      .attr("viewBox", "0 0 1260 800")
      .style('transform', 'translate(10%, 10%)')

      d3.select("div#mapd3")
      .append("div")
      .attr("id", "tooltip");
  }

  componentDidUpdate(){
    var stats = this.props.info.stats;
    var color = this.props.color;
    if (color === undefined)
      color = ["#fff2e6", "#e67300"]

    if (stats !== undefined){
      stats["TX"]['color'] = d3.interpolate(color[0], color[1])(5);
      drawMap("#statesvg", stats, tooltipHtml, "#tooltip");
    }
  }

  render(){
    return (
        <div id={"mapd3"}></div>
    )
  }
}
