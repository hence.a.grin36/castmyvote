/*import React, {Component} from 'react';
import UsaMap from './UsaMap'

class UsaMapVis2 extends Component {
  constructor(props) {
    super(props);
    /*this.state= {
      "Mississippi": {Districts: 24 },
      "Illinois": {Districts: 3 },
      "New Mexico": {Districts: 7 },
      "Alabama": {Districts: 9 },
      "Texas": {Districts: 21 },
      "Louisiana": {Districts: 6 },
      "Kentucky": {Districts: 12 },
      "Michigan": {Districts: 9 },
      "Oregon": {Districts: 1 },
      "Montana": {Districts: 1 },
      "New York": {Districts: 1 },
      "Arkansas": {Districts: 6 },
      "Ohio": {Districts: 7 },
      "Georgia": {Districts: 8 },
      "South Dakota": {Districts: 3 },
      "New Jersey": {Districts: 2 },
      "South Carolina": {Districts: 3 },
      "Arizona": {Districts: 6 },
      "California": {Districts: 3 },
      "Colorado": {Districts: 1 },
      "Oklahoma": {Districts: 2 },
      "Alaska": {Districts: 1 },
      "Pennsylvania": {Districts: 1 },
      "Missouri": {Districts: 1 },
      "North Dakota": {Districts: 1 },
      "Indiana": {Districts: 1 }
    }
    this.state={
      texas: {
        "stats": {   
            "TX": { 
                "Governor ": "Greg Abbott",
                "L.Governor ": "Dan Patrick",
                "Population ": "2,870,4330",
                "Largest City ": "Houston",
                "Race": "-----",
                "White": "74.8%",
                "Black": "11.9%",
                "Asian": "4.4%",
                "Other": "8.8%"
            }
        }
    },
  }
}

  render(){
    let image = {
        background: 'transparent',
    }

    return(
      <div>
        <div className="texas-map" >
          <div className="jumbotron p-3 p-md-5 rounded" style={image}>
            <div className ="jumbotron_wrapper container">
              <h1><span id="d3text">USA MAP</span></h1>
              <h6><span id="d3text">Click on Texas for more info</span></h6>
              <UsaMap info={this.state.texas}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default UsaMapVis2;*/

import React, {Component} from 'react';
import UsaMap from './UsaMap'

class UsaMapVis2 extends Component {
  constructor(props) {
    super(props);
    this.state= {
      texas: {},
    }
  }
  
  componentWillMount() {
    fetch('http://api.castmyvote.me/map2')
      .then((resp) => resp.json()) 
      .then(data => this.setState({texas: data}))
      }

  render(){
    let image = {
        background: 'transparent',
    }

    return(
      <div>
        <div className="texas-map" >
          <div className="jumbotron p-3 p-md-5 rounded" style={image}>
            <div className ="jumbotron_wrapper container">
              <h1><span id="d3text">POVERTY DISTRICTS BY STATE</span></h1>
              <UsaMap info={this.state.texas}/>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default UsaMapVis2;

