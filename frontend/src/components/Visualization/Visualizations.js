import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './Visualizations.css';
import GraphsImg from '../../assets/images/graphs.svg';

class Visualizations extends Component {
  render() {
    return (
      <div className="Visualization">
          <div className="row">
            <div className="col-lg-12">
              <div className="container">
                <img src={GraphsImg} id="graphs" alt=""/>
                <h1 className="font-weight-bold">Visualizations</h1>
                <div className="row">
                  <div className="col-md-6">
                    <h5>Cast My Vote</h5>
                    <div className="items">
                      <div>
                        <Link to='/map' className="btn btn-primary">
                          United States Map
                        </Link>
                      </div>
                      <div>
                        <Link to='/bubble2' className="btn btn-primary">
                          Candidates Per Party
                        </Link>
                      </div>
                      <div>
                        <Link to='/bubble3' className="btn btn-primary">
                          Texas Demographics
                        </Link>
                      </div>
                      <div>
                        <Link to='/bubble' className="btn btn-primary">
                          District Populations
                        </Link>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <h5>Equal Education</h5>
                    <div className="items">
                      <div>
                        <Link to='/map2' className="btn btn-primary">
                          Poverty Map
                        </Link>
                      </div>
                      <div>
                        <Link to='/bubble4' className="btn btn-primary">
                          School Districts in Poverty
                        </Link>
                      </div>
                      <div>
                        <Link to='/povertypie' className="btn btn-primary">
                          Extreme Poverty Nationally
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    );
  }
}

export default Visualizations;
