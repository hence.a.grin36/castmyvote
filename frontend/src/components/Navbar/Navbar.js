import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';

class Navbar extends Component {
  render() {
    return (
      <div className="Navbar">
        <nav className="navbar navbar-expand-lg navbar-light">
          <a className="navbar-brand" href="/">Cast My Vote</a>
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbar">
            <ul className="navbar-nav mr-auto">
            <li className="nav-item">
                <Link to='/visualizations' className="nav-link">
                  Visualizations
                </Link>
              </li>
              <li className="nav-item">
                <Link to='/search' className="nav-link">
                  Search
                </Link>
              </li>
              <li className="nav-item">
                <Link to='/elections' className="nav-link">
                  Elections
                </Link>
              </li>
              <li className="nav-item">
                <Link to='/candidates' className="nav-link">
                  Candidates
                </Link>
              </li>
              <li className="nav-item">
                <Link to='/locations' className="nav-link">
                  Locations
                </Link>
              </li>
              <li className="nav-item">
                <Link to='/about' className="nav-link">
                  About
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      </div>
    );
  }
}

export default Navbar;
