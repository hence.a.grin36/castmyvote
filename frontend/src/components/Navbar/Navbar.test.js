import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Navbar from './Navbar';
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {shallow} from 'enzyme';
import { Link } from 'react-router-dom';

configure({ adapter: new Adapter() });

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
  	<BrowserRouter>
  		<Navbar />
  	</BrowserRouter>
  , div);
  ReactDOM.unmountComponentAtNode(div);
});

// it('Check that the links exist', () => {
//   const navbar = shallow(<Navbar/>);
//   expect(navbar.find('')).to.have.lengthOf(5);
// });