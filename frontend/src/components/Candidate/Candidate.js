import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import { FacebookProvider, Page } from 'react-facebook';
import { TwitterTimelineEmbed } from 'react-twitter-embed';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPhone, faEnvelope, faDesktop } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faTwitter } from '@fortawesome/free-brands-svg-icons';
import './Candidate.css';

class Candidate extends Component {
  constructor() {
    super();
    this.state = {
      candidate: {}
    }

    this.renderLink = this.renderLink.bind(this);
    this.renderInfo = this.renderInfo.bind(this);
    
    this.renderContactMethod = this.renderContactMethod.bind(this);
    this.renderContact = this.renderContact.bind(this);
  }

  componentWillMount() {
    const { id } = this.props.match.params

    let url = "http://api.castmyvote.me/candidates/" + id;

    Axios.get(url)
      .then(response => response.data)
      .then(data => this.setState({candidate: data}))
  }

  renderLink(value, url) {
    return value && value.map(val => {
      return <Link to={url}>{val}</Link>
    })
  }

  renderInfo(key, value) {
    return (
      <div className="candidate-info">
        <strong>{key + ":"}</strong><br/>{value ? value : <i>none</i>}
      </div>
    )
  }

  renderContactMethod(key, value, i) {
    switch (key) {
      case "phone":
        return (
          <div className="contact-info" key={i}>
            <FontAwesomeIcon icon={faPhone} rotation={90}/>
            <span>{value}</span>
          </div>
        )
      case "website":
        return (
          <div className="contact-info" key={i}>
            <FontAwesomeIcon icon={faDesktop}/>
            <span><a href={"http://" + value}>{value}</a></span>
          </div>
        )
      case "emailid":
        return (
          <div className="contact-info" key={i}>
            <FontAwesomeIcon icon={faEnvelope}/>
            <span><a href={"mailto: " + value}>{value}</a></span>
          </div>
        )
      case "facebook":
        return (
          <div className="contact-info" key={i}>
            <FontAwesomeIcon icon={faFacebook}/>
            <span><a href={value}>{value}</a></span>
          </div>
        )
      case "twitter":
        return (
          <div className="contact-info" key={i}>
            <FontAwesomeIcon icon={faTwitter}/>
            <span><a href={"https://twitter.com/" + value.substring(value.indexOf("@") + 1)}>{value}</a></span>
          </div>
        )
      default:
        return ""
    }
  }

  renderContact(contact) {
    return contact && Object.keys(contact).map((c, i) => {
      return this.renderContactMethod(c, contact[c], i)
    })
  }

  renderElections(elections) {
    return elections && elections.map(election => {
      return (
        <div key={election.id}>
          <Link to={"/elections/" + election.id}>
            {election.name}
          </Link>
        </div>
      )
    })
  }

  renderLocation(location) {
    return location && (
      <Link to={"/locations/" + location.id}>
        {location.name}
      </Link>
    )
  }

  render() {
    const candidate = this.state.candidate;

    return (
      <div className="Candidate">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h6 className="text-muted text-left mb-0">{candidate.incumbent ? "Incumbent" : "Candidate"}</h6>
              <h1 className="section-header mb-5">{candidate.name}</h1>
              <div className="container">
                <div className="row">
                  <div className="col-md-4">
                    <img className="candidate-image" src={candidate.image} alt={candidate.name} />
                  </div>
                  <div className="col-md-8">
                    <div className="row text-left">
                      <div className="col-md-5 px-2">
                        <div className="card mb-2">
                          <div className="card-body">
                            {this.renderInfo("Current Office", candidate.current_office)}
                          </div>
                        </div>
                        <div className="card my-2">
                          <div className="card-body">
                            {this.renderInfo("Party", candidate.party)}
                          </div>
                        </div>
                        <div className="card my-2">
                          <div className="card-body">
                            {this.renderInfo("Running For", candidate.running_office)}
                          </div>
                        </div>
                        <div className="card my-2">
                          <div className="card-body">
                            {this.renderInfo("Location", this.renderLocation(candidate.location))}
                          </div>
                        </div>
                      </div>
                      <div className="col-md-7 px-2">
                        <div className="card mb-2">
                          <div className="card-body">
                            {this.renderInfo("Elections", this.renderElections(candidate.elections))}
                          </div>
                        </div>
                        <div className="card my-2">
                          <div className="card-body">
                            {this.renderInfo("Contact", this.renderContact(candidate.contact_info))}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row my-5">
                  <div className="col m-2 text-center">
                    {
                      candidate.contact_info && candidate.contact_info.facebook ? 
                      <FacebookProvider appId="1405750502892949">
                        <Page href={candidate.contact_info.facebook} tabs="timeline" width={500}/>
                      </FacebookProvider> : ""
                    }
                    {
                      candidate.contact_info && candidate.contact_info.twitter ?
                      <TwitterTimelineEmbed
                        sourceType="profile"
                        screenName={candidate.contact_info.twitter.substring(candidate.contact_info.twitter.indexOf('@') + 1)}
                        options={{height: 500, width: 500}}
                      /> : ""
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Candidate;
