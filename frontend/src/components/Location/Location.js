import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Axios from 'axios';
import { PieChart, Pie, Cell, Tooltip } from 'recharts';
import './Location.css';

class Location extends Component {
  constructor() {
    super();
    this.state = {
      location: {}
    }

    this.renderLink = this.renderLink.bind(this);
    this.renderInfo = this.renderInfo.bind(this);
  }

  componentWillMount() {
    const { id } = this.props.match.params

    let url = "http://api.castmyvote.me/locations/" + id;

    Axios.get(url)
      .then(response => response.data)
      .then(data => this.setState({location: data}))
  }

  formatPopulation(population) {
    if (population) {
      return population.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
  }

  renderLink(value, url) {
    return value && value.map(val => {
      return <Link to={url}>{val}</Link>
    })
  }

  renderInfo(key, value) {
    return (
      <div className="location-info">
        <strong>{key + ":"}</strong><br/>{value ? value : <i>not available</i>}
      </div>
    )
  }

  renderPopulation(characteristic) {
    return characteristic && Object.keys(characteristic).map((c, i) => {
      return (
        <p className="mb-0" key={i}>{c.charAt(0).toUpperCase() + c.slice(1)}: {characteristic[c]}</p>
      )
    })
  }

  renderVotingAgePieChart(votingAge) {
    if (votingAge) {
      const votingAgePercentage = votingAge.substring(0, votingAge.indexOf('%'))
      const complement = (100 - parseFloat(votingAgePercentage)).toFixed(1)

      const data = {
        "able to vote": votingAgePercentage,
        "unable to vote": complement
      }

      return this.renderPieChart(data)
    }
  }

  renderPieChart(characteristic) {
    if (!characteristic) {
      return;
    }

    const RADIAN = Math.PI / 180;                    
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }) => {
      const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
      const x = cx + radius * Math.cos(-midAngle * RADIAN);
      const y = cy + radius * Math.sin(-midAngle * RADIAN);
     
      return (
        <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'}  dominantBaseline="central">
          {`${(percent * 100).toFixed(1)}%`}
        </text>
      );
    };  

    let data = []
    const colors = ["#1052bc", "#3d77d3", "#7aadff", "#a8c9ff", "#e0ecff"]

    Object.keys(characteristic).forEach((key, i) => {
      let temp = {}
      temp.name = key
      temp.value = parseFloat(characteristic[key])
      temp.color = colors[i]
      data.push(temp)
    })

    return (
      <PieChart width={250} height={250}>
        <Tooltip/>  
        <Pie
          data={data}
          dataKey="value"
          cx={125} 
          cy={125} 
          labelLine={false}
          label={renderCustomizedLabel}
          outerRadius={100} 
          fill="#8884d8"
        >
          {data.map((entry, i) => <Cell key={i} fill={colors[i]}/>)}
        </Pie>
      </PieChart>
    )
  }

  renderCandidates(candidates) {
    if (candidates && candidates.length > 0) {
      return candidates.map(candidate => {
        return (
          <div key={candidate.id}>
            <Link to={"/candidates/" + candidate.id}>
              {candidate.name}
            </Link>
          </div>
        )
      })
    } else {
      return <i>no candidates for this location</i>
    }
  }

  renderElections(elections) {
    if (elections && elections.length > 0) {
      return elections && elections.map(election => {
        return (
          <div key={election.id}>
            <Link to={"/elections/" + election.id}>
              {election.name}
            </Link>
          </div>
        )
      })
    } else {
      return <i>no elections in this location</i>
    }
  }

  render() {
    const location = this.state.location;

    return (
      <div className="Location">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <h6 className="text-muted text-left mb-0">Location</h6>
              <h1 className="section-header mb-5">{location.name}</h1>
              <div className="container">
                <div className="row">
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Incumbent", location.current_incumbent)}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Population", this.formatPopulation(location.population))}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col m-2 card text-left">
                    <div className="card-body population-info">
                      {this.renderInfo("Race Percentages", this.renderPopulation(location.race))}
                    </div>
                    <div className="card-body pie-chart">
                      {this.renderPieChart(location.race)}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body population-info">
                      {this.renderInfo("Gender Percentages", this.renderPopulation(location.gender))}
                    </div>
                    <div className="card-body pie-chart">
                      {this.renderPieChart(location.gender)}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body population-info">
                      {this.renderInfo("Voting Age", location.voting_age)}
                    </div>
                    <div className="card-body pie-chart">
                      {this.renderVotingAgePieChart(location.voting_age)}
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Candidates", this.renderCandidates(location.candidates))}
                    </div>
                  </div>
                  <div className="col m-2 card text-left">
                    <div className="card-body">
                      {this.renderInfo("Elections", this.renderElections(location.elections))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Location;
