import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import './Home.css';
import CallToActionImg from '../../assets/images/home.svg';

class Home extends Component {
  render() {
    return (
      <div className="Home">
          <div className="row">
            <div className="col-lg-6">
              <div className="container">
                <h1 className="font-weight-bold">Your vote matters</h1>
                <p className="text-muted">
                  Making your voice heard is more important than ever before,
                  but keeping up with local election dates and candidate standpoints
                  can be time-consuming and confusing. <br/>
                  We're here to help you learn more.
                </p>
                <br />
                <p className="text-muted">
                  Become an informed voter today!
                </p>
                <div className="buttons text-center">
                  <Link to='/elections' className="btn btn-primary">
                    Elections
                  </Link>
                  <Link to='/candidates' className="btn btn-primary">
                    Candidates
                  </Link>
                  <Link to='/locations' className="btn btn-primary">
                    Locations
                  </Link>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="container">
                <img src={CallToActionImg} id="call-to-action" alt=""/>
              </div>
            </div>
        </div>
      </div>
    );
  }
}

export default Home;
