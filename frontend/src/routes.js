import React from 'react';
import { Route } from 'react-router';

/**
 * Import all page components here
 */
import App from './components/App/App';
import Header from './components/Header/Header';
import Main from './components/Main/Main';
import Home from './components/Home/Home';

import Elections from './components/Elections/Elections';
import Election from './components/Election/Election';

import Candidates from './components/Candidates/Candidates';
import Candidate from './components/Candidate/Candidate';

import Locations from './components/Locations/Locations';
import Location from './components/Location/Location';

import About from './components/About/About';

import AllSearch from './components/AllSearch/AllSearch';
import D3Carousel from './components/Visualization/D3Carousel';

import Bubble from './components/Visualization/Bubble/Bubble';
import Bubble2 from './components/Visualization/Bubble/Bubble2';
import Bubble3 from './components/Visualization/Bubble/Bubble3';
import PovertyPie from './components/Visualization/PovertyPie';
/**
 * All routes go here.
 * Don't forget to import the components above after adding new route.
 */
export default (
  <Route path="/" component={App}>
  </Route>
);
