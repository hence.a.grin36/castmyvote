from selenium import webdriver
from unittest import main, TestCase


class TestCases(TestCase):
    def setUp(self):
        chromeOpts = webdriver.ChromeOptions()
        chromeOpts.add_argument('--headless')
        chromeOpts.add_argument('--disable-gpu')
        chromeOpts.add_argument('--no-sandbox')
        self.driver = webdriver.Chrome(options=chromeOpts)
        self.driver.get('http://castmyvote.me')

    # Test author: Aaditya Murthy
    def test_title(self):
        self.assertEqual(self.driver.title, 'Cast My Vote')

    # Test Author: Aaditya Murthy
    def test_brand(self):
        brand = self.driver.find_element_by_class_name('navbar-brand')
        brand.click()
        header_text = self.driver.find_element_by_xpath(
            "//*[@id='root']/div/main/div/div/div[1]/div/h1")
        self.assertEqual(header_text.text, 'Your vote matters')

    # Test Author: Aaditya Murthy
    def test_buttons(self):
        election_button = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][1]")
        candidate_button = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][2]")
        location_button = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][3]")
        self.assertTrue(
            election_button.is_displayed and candidate_button.is_displayed and location_button.is_displayed)
        self.assertEqual(election_button.text, 'Elections')
        self.assertEqual(candidate_button.text, 'Candidates')
        self.assertEqual(location_button.text, 'Locations')

    # Test Author: Aaditya Murthy
    def test_navbar(self):
        election_button = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][1]")
        candidate_button = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][2]")
        location_button = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][3]")
        self.assertTrue(
            election_button.is_displayed and candidate_button.is_displayed and location_button.is_displayed)
        self.assertEqual(election_button.text, 'Elections')
        self.assertEqual(candidate_button.text, 'Candidates')
        self.assertEqual(location_button.text, 'Locations')
        # election_button.click()

    # # Test Author: Aaditya Murthy
    # def test_elections(self):
    #     header = self.driver.find_element_by_class_name('section-header')
    #     self.assertEqual(header.text, 'Elections')
    #     brand = self.driver.find_element_by_class_name('navbar-brand')
    #     brand.click()

    # Test Author: Aaditya Murthy
    def test_candidates(self):
        candidate_button = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][2]")
        candidate_button.click()
        header = self.driver.find_element_by_class_name('section-header')
        self.assertEqual(header.text, 'CANDIDATES')
        brand = self.driver.find_element_by_class_name('navbar-brand')
        brand.click()

    def test_locations(self):
        locations = self.driver.find_element_by_xpath(
            "//a[@class='btn btn-primary'][3]")
        locations.click()
        header = self.driver.find_element_by_class_name('section-header')
        self.assertEqual(header.text, 'LOCATIONS')

    def tearDown(self):
        self.driver.quit()


if __name__ == "__main__":
    main()
