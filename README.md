# CastMyVote
[castmyvote.me](http://castmyvote.me/)

[API Documentation](https://documenter.getpostman.com/view/5462463/RWgm3ggs)

Git SHA: 35bda35d6efa2e0ea70c88c620c9dd35688a09a5

[Gitlab Pipelines](https://gitlab.com/hence.a.grin36/castmyvote/pipelines)

Members:
- Regina Chen (rc43758)
- Yujie (Jessie) Chen (yc24962)
- Mert Hizli (mh52252)
- Paulina Lee (yy6682)
- Aaditya Murthy (aam4786)
- Sarah Robertson (skr949)

Estimated time:
- Regina Chen: 20hr
- Yujie Chen: 25hr
- Mert Hizli: 27hr
- Paulina Lee: 20hr
- Aaditya Murthy: 22hr
- Sarah Robertson: 21hr

Actual time:
- Regina Chen: 28hr
- Yujie Chen: 29hr
- Mert Hizli: 27hr
- Paulina Lee: 29hr
- Aaditya Murthy: 26hr
- Sarah Robertson: 28hr

Comments: None

## Development
From the parent directory, run `docker-compose up`. This will start to build the docker images and will run both the frontend and backend.

If you were previously running outdated containers, run `docker-compose build` to rebuild, then `docker-compose up`. To up the containers in the background, use `docker-compose up -d`.

To stop both containers, use `docker-compose stop`.

To drop into the database container, use docker-compose to bring up all the containers, then use psql -h localhost -U root castmyvotedb and type "devpassword" when asked for the database password. Then, type `\dt` to show all the tables. You can then run normal SQL queries like `select * from candidate;`

### + Running specific containers
If you want to only run the frontend or backend containers, `cd` into the appropriate folder and do the following:

1. Run `docker build .`. If you would like to name the image instead of having to use the image ID, run `docker build -t [name] .` instead.
2. Run `docker run [image ID or tag]`.
3. To run the container in the background, add the flag `-d` like so: `docker run [image ID or tag] -d`
4. To stop the container, `Ctrl + C` or use `docker stop [container ID]`. The container ID can be obtained by using `docker ps`.

### + Other useful Docker commands
- View running and stopped containers: `docker ps -a`
- Remove stopped containers: `docker container prune`
- Remove unused images: `docker image prune`

## Deployment
The API and frontend are deployed to individual single-docker EC2 instances through Elastic Beanstalk. The deploy process is like so:

1. Zip together the correct directory (frontend, backend)
2. Upload to a temporary staging environment on EB to verify that it is working as expected
3. Deploy to production

The staging environments will be shut down after checks to save compute resources and stay within the free tier limits if possible.

## Misc.
Frontend created with `npm install -g create-react-app` and `npm install --save bootstrap`
