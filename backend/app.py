from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api
from flask_cors import CORS
from sqlalchemy import or_, cast, String, desc, asc
from pathlib import Path
import pickle

import json
import models
from models import Election, Candidate, Location
from models import *

import os

app = Flask(__name__)
cors = CORS(app)

production = os.environ.get('PRODUCTION', None)
if not production:
    db_name = os.environ.get('DEV_DB_NAME')
    db_hostname = os.environ.get('DEV_HOSTNAME')
    db_username = os.environ.get('DEV_USERNAME')
    db_password = os.environ.get('DEV_PASSWORD')
    base_url = 'http://localhost:5000'
else:
    db_name = os.environ.get('RDS_DB_NAME')
    db_hostname = os.environ.get('RDS_HOSTNAME')
    db_username = os.environ.get('RDS_USERNAME')
    db_password = os.environ.get('RDS_PASSWORD')
    base_url = 'http://api.castmyvote.me'

if db_name:
    app.config['SQLALCHEMY_DATABASE_URI'] = f'postgresql://{db_username}:{db_password}@{db_hostname}/{db_name}'

app.config['SQLALCHEMY_BINDS'] = None
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JSON_SORT_KEYS'] = False
# enzyme, jest for frontend unit testing (don't use mocha)

PAGE_SIZE = 16

with app.app_context():
    models.create(app)

api = Api(app)

@app.route('/')
def home():
    return jsonify({'status': 'OK'})

@app.route('/createdb')
def create_db():
    models.populate_db()
    return jsonify({'info': 'database created successfully'})

@app.route('/redeploydrop')
def delete_db():
    models.drop_db()
    return jsonify({'info': 'database deleted, ready for redeploy'})

@app.route('/randomize')
def clear():
    models.regen_images()
    return jsonify({'info': 'images updated'})

@app.route('/elections')
@app.route('/elections/')
def get_all_elections():
    pagenum = request.args.get('page') or '1'
    sort_by = request.args.get('sort')
    search_by = request.args.get('search')
    order_by = request.args.get('order')
    filter_by = request.args.get('type')
    page_url = f'{base_url}/elections?'

    # validate query params
    valid_sort = {"name", "date", "election_type", "office_name"}
    valid_order = ["asc", "desc"]
    valid_filters = {
        "tx_house": "TX-H",
        "tx_senate": "TX-S",
        "us_house": "US-H",
        "us_senate": "US-S",
        "texas": "TX"
    }

    # apply sorting, searching and filters
    url_params = []
    results = Election.query
    if sort_by and sort_by.lower() in valid_sort:
        sort_by = sort_by.lower()
        # Determine order of sort
        if order_by and order_by.lower() == valid_order[1]:
            results = results.order_by(desc(getattr(Election, sort_by)))
        else:
            results = results.order_by(asc(getattr(Election, sort_by)))
        url_params.append(f'sort={sort_by}')

        # Append order by attribute only if it is in valid orders
        if order_by and order_by.lower() in valid_order:
            url_params.append(f'order={order_by.lower()}')
    if filter_by and filter_by.lower() in valid_filters:
        filter_by = filter_by.lower()
        results = results.filter_by(classification=valid_filters[filter_by])
        url_params.append(f'type={filter_by}')
    if search_by:
        name_filter = '%' + search_by + '%'
        #by_location = Election.query.join(Location).filter(Location.name.ilike(name_filter))
        by_candidate = Election.query.join(Candidate,Election.election_candidates).filter(Candidate.name.ilike(name_filter))
        results = results.filter(or_(Election.name.ilike(name_filter),Election.office_name.ilike(name_filter),Election.date.ilike(name_filter),Election.election_type.ilike(name_filter)))
        results =results.union(by_candidate)

        url_params.append(f'search={search_by}')

    # turn into paginated object
    query_params = '&' + '&'.join(url_params) if url_params else ''
    pagination = results.paginate(page=int(pagenum), per_page=PAGE_SIZE)
    prev_url = page_url + f'page={pagination.prev_num}' + query_params if pagination.prev_num else None
    next_url = page_url + f'page={pagination.next_num}' + query_params if pagination.next_num else None
    data = {
        'count': pagination.total,
        'total_pages': pagination.pages,
        'current_page': pagination.page,
        'previous': prev_url,
        'next': next_url,
        'sort_by': sort_by,
        'results': [e.as_dict(link_candidates=True, link_locations=True) for e in pagination.items]
    }
    return jsonify(data)

@app.route('/elections/<election_id>')
def get_election(election_id):
    return jsonify(Election.query.filter_by(id=election_id).first().as_dict(link_candidates=True, link_locations=True))

@app.route('/candidates')
@app.route('/candidates/')
def get_all_candidates():
    pagenum = request.args.get('page') or '1'
    sort_by = request.args.get('sort')
    search_by = request.args.get('search')
    order_by = request.args.get('order')
    filter_by = request.args.get('incumbent') or request.args.get('party')
    page_url = f'{base_url}/candidates?'

    valid_sort = {"name", "party", "incumbent", "running_office", "current_office"}
    valid_order = ["asc", "desc"]
    valid_filters = {
        'true': True,
        'false': False,
        'republican': 'Republican',
        'democrat': 'Democrat',
        'independent': 'Independent',
        'libertarian': 'Libertarian'
    }
    url_params = []
    results = Candidate.query
    if sort_by and sort_by.lower() in valid_sort:
        sort_by = sort_by.lower()
        # Determine order of sort
        if order_by and order_by.lower() == valid_order[1]:
            results = results.order_by(desc(getattr(Candidate, sort_by)))
        else:
            results = results.order_by(asc(getattr(Candidate, sort_by)))
        url_params.append(f'sort={sort_by}')

        # Append order by attribute only if it is in valid orders
        if order_by and order_by.lower() in valid_order:
            url_params.append(f'order={order_by}')
    if filter_by and filter_by.lower() in valid_filters:
        filter_by = filter_by.lower()
        if filter_by == 'true' or filter_by == 'false':
            results = results.filter_by(incumbent=valid_filters[filter_by])
            url_params.append(f'incumbent={filter_by}')
        else:
            results = results.filter_by(party=valid_filters[filter_by])
            url_params.append(f'party={filter_by}')
    if search_by:
        name_filter = '%' + search_by + '%'
        #Candidate.location in Location.query.filter(Location.name.ilike(name_filter)).with_entities(Location.id).all()
        by_location = Candidate.query.join(Location).filter(Location.name.ilike(name_filter))
        #by_election = Candidate.query.join(Election,Candidate.elections).filter(Election.name.ilike(name_filter))
        results = results.filter(or_(Candidate.name.ilike(name_filter),Candidate.party.ilike(name_filter),Candidate.running_office.ilike(name_filter), Candidate.current_office.ilike(name_filter)))
        results = results.union(by_location)
        url_params.append(f'search={search_by}')

    query_params = '&' + '&'.join(url_params) if url_params else ''
    pagination = results.paginate(page=int(pagenum), per_page=PAGE_SIZE)
    prev_url = page_url + f'page={pagination.prev_num}' + query_params if pagination.prev_num else None
    next_url = page_url + f'page={pagination.next_num}' + query_params if pagination.next_num else None
    data = {
        'count': pagination.total,
        'total_pages': pagination.pages,
        'current_page': pagination.page,
        'previous': prev_url,
        'next': next_url,
        'sort_by': sort_by,
        'results': [e.as_dict(link_elections=True, link_locations=True) for e in pagination.items]
    }
    return jsonify(data)

@app.route('/candidates/<candidate_id>')
def get_candidate(candidate_id):
    return jsonify(Candidate.query.filter_by(id=candidate_id).first().as_dict(link_elections=True, link_locations=True))

@app.route('/locations')
@app.route('/locations/')
def get_all_locations():
    pagenum = request.args.get('page') or '1'
    sort_by = request.args.get('sort')
    search_by = request.args.get('search')
    order_by = request.args.get('order')
    filter_by = request.args.get('population')
    page_url = f'{base_url}/locations?'

    valid_sort = {"name", "population"}
    valid_order = ["asc", "desc"]
    valid_filters = {
        "100000": 100000,
        "200000": 200000,
        "500000": 500000,
        "800000": 800000,
        "1000000": 1000000,
    }

    url_params = []
    results = Location.query
    if sort_by and sort_by.lower() in valid_sort:
        sort_by = sort_by.lower()
        # Determine order of sort
        if order_by and order_by.lower() == valid_order[1]:
            results = results.order_by(desc(getattr(Location, sort_by)))
        else:
            results = results.order_by(asc(getattr(Location, sort_by)))
        url_params.append(f'sort={sort_by}')

        # Append order by attribute only if it is in valid orders
        if order_by and order_by.lower() in valid_order:
            url_params.append(f'order={order_by}')
    if filter_by and filter_by.lower() in valid_filters:
        filter_by = filter_by.lower()
        results = results.filter(Location.population>=valid_filters[filter_by])
        url_params.append(f'population={filter_by}')
    if search_by:
        name_filter = '%' + search_by + '%'
        #by_candidates = Location.query.join(Candidate,Location.candidates).filter(Candidate.name.ilike(name_filter))
        by_election = Location.query.join(Election,Location.elections).filter(Election.name.ilike(name_filter))
        results = results.filter(or_(Location.name.ilike(name_filter),Location.current_incumbent.ilike(name_filter),Location.voting_age.ilike(name_filter), cast(Location.population, String).ilike(name_filter)))
        results = results.union(by_election)
        url_params.append(f'search={search_by}')

    query_params = '&' + '&'.join(url_params) if url_params else ''
    pagination = results.paginate(page=int(pagenum), per_page=PAGE_SIZE)
    prev_url = page_url + f'page={pagination.prev_num}' + query_params if pagination.prev_num else None
    next_url = page_url + f'page={pagination.next_num}' + query_params if pagination.next_num else None
    data = {
        'count': pagination.total,
        'total_pages': pagination.pages,
        'current_page': pagination.page,
        'previous': prev_url,
        'next': next_url,
        'sort_by': sort_by,
        'results': [e.as_dict(link_elections=True, link_candidates=True) for e in pagination.items]
    }
    return jsonify(data)

@app.route('/locations/<location_id>')
def get_location(location_id):
    return jsonify(Location.query.filter_by(id=location_id).first().as_dict(link_elections=True, link_candidates=True))


@app.route('/searchall')
@app.route('/searchall/')
def search_all():
    def find_object(name):
        if Location.query.filter_by(name=name).first() is not None:
            return Location.query.filter_by(name=name).first().as_dict(link_elections=True, link_candidates=True)
        if Election.query.filter_by(name=name).first() is not None:
            return Election.query.filter_by(name=name).first().as_dict(link_candidates=True, link_locations=True)
        if Candidate.query.filter_by(name=name).first() is not None:
            return Candidate.query.filter_by(name=name).first().as_dict(link_elections=True, link_locations=True)

    pagenum = request.args.get('page') or '1'
    search_by = request.args.get('search')
    page_url = f'{base_url}/searchall?'
    all_results = None

    results1 = Location.query.with_entities(Location.name)
    results2 = Election.query.with_entities(Election.name)
    results3 = Candidate.query.with_entities(Candidate.name)
    url_params = []

    if search_by:
        name_filter = '%' + search_by + '%'
        result1 = results1.filter(Location.name.ilike(name_filter))
        result2 = results2.filter(Election.name.ilike(name_filter))
        result3 = results3.filter(Candidate.name.ilike(name_filter))
        all_results = result1.union(result2,result3)
        url_params.append(f'search={search_by}')

    query_params = '&' + '&'.join(url_params) if url_params else ''
    pagination = all_results.paginate(page=int(pagenum), per_page=PAGE_SIZE)
    prev_url = page_url + f'page={pagination.prev_num}' + query_params if pagination.prev_num else None
    next_url = page_url + f'page={pagination.next_num}' + query_params if pagination.next_num else None
    data = {
        'count': pagination.total,
        'total_pages': pagination.pages,
        'current_page': pagination.page,
        'previous': prev_url,
        'next': next_url,
        'results': [find_object(e[0]) for e in pagination.items]
    }
    return jsonify(data)

@app.route('/map')
@app.route('/map/')
def map():
    with open('texas-data', 'rb') as f:
        data = json.load(f)
    return jsonify(data)

@app.route('/map2')
@app.route('/map2/')
def map2():
    with open('state-data', 'rb') as f:
        data = json.load(f)
    return jsonify(data)

@app.route('/bubble')
@app.route('/bubble/')
def bubble():
    with open('al_loc.json', 'rb') as f:
        data = json.load(f)
    return jsonify(data)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
