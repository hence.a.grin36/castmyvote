# pylint: disable=no-member

import json
import random
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from itertools import chain

db = SQLAlchemy()

race_details = db.Table('race_details',
    db.Column('candidate_id', db.Integer, db.ForeignKey('candidate.id')),
    db.Column('election_id', db.Integer, db.ForeignKey('election.id')))

class Candidate(db.Model):
    """
    json data:
    {
        "Name": "Greg Abbott",
        "Party": "Republican",
        "Profile Link": "http://texascandidates.com/pages/CandidateDetails.aspx?CandidateId=4a00e813-9226-4ae6-8ec4-dd3520fbe14b",
        "Incumbent": true,
        "Location": "Texas",
        "Running Office": "Governor",
        "Election": "General Midterm Election - Governor",
        "Current Office": "Governor ",
        "Contact": {
            "Phone": "(512) 463 - 2000"
        },
        "Image": "https://api.ballotpedia.org/v3/thumbnail/200/300/crop/best/GregAbbott2015.jpg"
    }
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=False)
    party = db.Column(db.String(120), unique=False, nullable=False)
    incumbent = db.Column(db.Boolean, unique=False, nullable=False)
    running_office = db.Column(db.String(80), unique=False, nullable=False)
    current_office = db.Column(db.String(80), unique=False, nullable=True)
    image = db.Column(db.String(300), unique=False, nullable=False)

    location = db.Column(db.Integer, db.ForeignKey('location.id'))

    # set here so elections should be able to backref with election.candidates
    elections = db.relationship('Election', secondary=race_details, lazy='subquery',
        backref=db.backref('election_candidates', lazy=True))
    # contact_info = db.relationship('ContactInfo', backref='contact_candidate', uselist=False)
    contact_info = db.Column(db.PickleType, nullable=True)

    def __init__(self, name, party, incumbent, running_office, current_office, image=None, contact_info=None):
        self.name = name
        self.party = party
        self.incumbent = incumbent
        self.running_office = running_office
        self.current_office = current_office
        self.image = image
        self.contact_info = contact_info

    def __repr__(self):
        return '<Candidate %r>' % (self.name)
    
    def as_dict(self, link_elections=False, link_locations=False):
        jsondict = {
            'id': self.id,
            'name': self.name,
            'party': self.party,
            'incumbent': self.incumbent,
            'running_office': self.running_office,
            'current_office': self.current_office,
            'image': self.image,
            'contact_info': self.contact_info
        }
        if link_elections:
            jsondict['elections'] = [election.as_dict() for election in self.elections]
        if link_locations:
            jsondict['location'] = self.candidate_location.as_dict() if self.candidate_location else None
        return jsondict
    
class Election(db.Model):
    """
    json data:
    {
        "Election Name": "General Midterm Election - Governor",
        "Office Name": "Governor",
        "Date": "November 6th, 2018",
        "Type": "General",
        "Location": "Texas",
        "Upcoming": true,
        "Candidates": [
            "Greg Abbott",
            "Mark Tippetts",
            "Lupe Valdez"
        ],
        "Winner": null
    }
    """
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=True, nullable=False)
    office_name = db.Column(db.String(150), unique=False, nullable=False)
    date = db.Column(db.String(150), unique=False, nullable=False)
    election_type = db.Column(db.String(80), unique=False, nullable=False)
    upcoming = db.Column(db.Boolean, unique=False, nullable=False)
    classification = db.Column(db.String(30), unique=False, nullable=False)
    winner = db.Column(db.String(80), unique=False, nullable=True)
    image = db.Column(db.String(300), unique=False, nullable=False)

    location = db.Column(db.Integer, db.ForeignKey('location.id'))

    def __init__(self, name, office_name, date, election_type, upcoming, classification,
        winner=None, image=None):
        self.name = name
        self.office_name = office_name
        self.date = date
        self.election_type = election_type
        self.upcoming = upcoming
        self.winner = winner
        self.image = image
        self.classification = classification

    def __repr__(self):
        return '<Election %r>' % (self.name)

    def as_dict(self, link_candidates=False, link_locations=False):
        """
        link_candidates/link_locations refers to options to display nested info for
        foreign keys of this model. this is turned off by default so that for example,
        when the election object references this model, it doesn't reprint the election
        and cause a loop of nested models
        """
        jsondict = {
            'id': self.id,
            'name': self.name,
            'office_name': self.office_name,
            'date': self.date,
            'election_type': self.election_type,
            'upcoming': self.upcoming,
            'winner': self.winner,
            'image': self.image
        }
        if link_candidates:
            jsondict['candidates'] = [candidate.as_dict() for candidate in self.election_candidates]
        if link_locations:
            jsondict['location'] = self.election_location.as_dict() if self.election_location else None
        return jsondict

class Location(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(200), unique=True, nullable=False)
    current_incumbent = db.Column(db.String(200), unique=True, nullable=True)
    gender = db.Column(db.PickleType, nullable=True)
    race = db.Column(db.PickleType, nullable=True)
    ethnicity = db.Column(db.PickleType, nullable=True)
    voting_age = db.Column(db.String(200), nullable=True)
    population = db.Column(db.Integer, nullable=True)

    candidates = db.relationship('Candidate', backref='candidate_location', lazy='dynamic')
    elections = db.relationship('Election', backref='election_location', lazy='dynamic')
    def __init__(self, info_dict):
        for key in info_dict:
            if info_dict[key] != 'Vacant':
                setattr(self, key, info_dict[key])

    def as_dict(self, link_candidates=False, link_elections=False):
        jsondict = {
            'id': self.id,
            'name': self.name,
            'current_incumbent': self.current_incumbent,
            'population': self.population,
            'race': self.race,
            'ethnicity': self.ethnicity,
            'voting_age': self.voting_age,
            'gender': self.gender
        }
        if link_candidates:
            jsondict['candidates'] = [candidate.as_dict() for candidate in self.candidates]
        if link_elections:
            jsondict['elections'] = [election.as_dict() for election in self.elections]
        return jsondict

def create(app):
    db.init_app(app)
    db.create_all()

def drop_db():
    db.reflect()
    db.drop_all()

defaults = {
    'https://images.unsplash.com/photo-1523739542410-74fa592fcd4a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1036df601d6dd7aee79461d6a20627c7&auto=format&fit=crop&w=668&q=80',
    'https://images.unsplash.com/photo-1499696562547-dbe51006e1bb?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f9438c7e1307947b3064994f83027791&auto=format&fit=crop&w=666&q=80',
    'https://images.unsplash.com/photo-1496112872005-7bb0b50dad9c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=585dd438576f49c12554dd2d8b587380&auto=format&fit=crop&w=1651&q=80',
    'https://images.unsplash.com/photo-1521214097642-d018b17afe31?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=27aaf10657e2eb0a6ed1349075e351de&auto=format&fit=crop&w=750&q=80',
    'https://images.unsplash.com/photo-1473167052083-3d31fa1f6776?ixlib=rb-0.3.5&s=76287535598316ad8d5f10c69ae1ab8a&auto=format&fit=crop&w=2917&q=80',
    'https://statesymbolsusa.org/sites/statesymbolsusa.org/files/primary-images/bigstock-Curious-Bald-Eagle-60608882.jpg',
    'https://s2.15min.lt/static/cache/OTI1eDYxMCw5ODR4NTIwLDYxNjY1MyxvcmlnaW5hbCwsaWQ9MTQ2NjQ1NiZkYXRlPTIwMTUlMkYwNiUyRjE5LDMwMjY5NDEwOTE=/erelis-jav-veliavos-fone-5583c355790f5.jpg',
    'https://www.brookings.edu/wp-content/uploads/2016/06/congress006-1.jpg',
    'https://freetoursbyfoot.com/wp-content/uploads/2014/02/washington-monument.jpg',
    'https://www.nationalparks.org/sites/default/files/styles/diptych_image_first/public/statue-of-liberty-vgnt.jpg?itok=htP3WegC',
    'https://images.unsplash.com/photo-1514850323273-a44696417fca?ixlib=rb-0.3.5&s=c06016bb5fa79e7c4ae1f4a6354abf7d&auto=format&fit=crop&w=2685&q=80',
    'https://images.unsplash.com/photo-1492217072584-7ff26c10eb75?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=50dc3d155b9999cc13d4a8143ad61503&auto=format&fit=crop&w=668&q=80',
    'https://images.unsplash.com/photo-1519922838705-9d6cb8bcfaea?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=be97aab8fa3461a8a58c2f96bd587956&auto=format&fit=crop&w=2760&q=80',
    'https://images.unsplash.com/photo-1524633712235-22da046738b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5eb308ff5c7d3017cbec6f92112f0f6e&auto=format&fit=crop&w=2800&q=80',
    'https://images.unsplash.com/photo-1496112872005-7bb0b50dad9c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=585dd438576f49c12554dd2d8b587380&auto=format&fit=crop&w=2851&q=80',
    'https://i.pinimg.com/originals/48/7b/36/487b366c0492081621a6949478e96c28.jpg',
    'https://s3.pixers.pics/pixers/700/FO/98/12/48/6/700_FO9812486_3bbd998c4afa30c976f6c2df59186d05.jpg',
    'https://images.unsplash.com/photo-1535738754398-da14b0594de3?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e0bac6fb3260670461d1f321e4b3fd05&auto=format&fit=crop&w=668&q=80',
    'https://images.unsplash.com/photo-1492309436847-61fcb5871410?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d2688bd4dbe13746b382609f7237cbc3&auto=format&fit=crop&w=2734&q=80',
    'https://images.unsplash.com/photo-1515040242872-08257d6d08c2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a68917a2019c9918143cac0703471519&auto=format&fit=crop&w=2850&q=80',
    'https://images.unsplash.com/photo-1494449880320-18d3dae5d16e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=08e65abb35feb276558df5fef46ccbfa&auto=format&fit=crop&w=668&q=80'
}

def populate_db():
    candidates_file = open('candidates.json', 'r')
    elections_file = open('elections.json', 'r')

    all_candidates = json.load(candidates_file)
    all_elections = json.load(elections_file)

    for c in all_candidates:
        # grab all values from dictionary that don't require another model
        if not c['image']:
            c['image'] = random.sample(defaults, 1)[0]
        candidate = Candidate(name=c['name'], party=c['party'], incumbent=c['incumbent'],
                 running_office=c['running_office'], current_office=c['current_office'],
                 image=c['image'], contact_info=c['contact'])
        db.session.add(candidate)
        db.session.commit()

    for e in all_elections:
        if not e['image']:
            e['image'] = random.sample(defaults, 1)[0]
        election = Election(name=e['election_name'], office_name=e['office_name'], date=e['date'],
                election_type=e['type'], upcoming=e['upcoming'], classification=e['classification'],
                winner=e['winner'], image=e['image'])
        for c in e['candidates']:
            candidate_model = Candidate.query.filter_by(name=c).first()
            election.election_candidates.append(candidate_model)
        
        db.session.add(election)
        db.session.commit()

    tx_senate_file = open('senate_districts.json', 'r')
    tx_senate_locations = json.load(tx_senate_file)

    tx_reps_file = open('rep_districts.json', 'r')
    tx_rep_locations = json.load(tx_reps_file)

    us_reps_file = open('us_rep_districts.json', 'r')
    us_rep_locations = json.load(us_reps_file)

    # model for just the state of TX, applies to statewide positions and US Senate position
    tx_info = {
        "name": "Texas",
        "current_incumbent": "Greg Abbott",
        "population": 28704330,
        "gender": {
            "female": "50.4%",
            "male": "49.6%"
        },
        "race": {
            "white": "74.8%",
            "black": "11.9%",
            "asian": "4.4%",
            "other": "8.8%"
        },
        "ethnicity": {
            "hispanic": "37%",
            "not_hispanic": "63%"
        },
        "voting_age": "71.8% age 18 and over",
        "next_general_election": "November 06, 2018"
    }
    texas_model = Location(tx_info)
    db.session.add(texas_model)
    db.session.commit()

    for l in chain(tx_senate_locations, tx_rep_locations, us_rep_locations):
        l['population'] = int(l['population'].replace(',',''))
        location = Location(l)
        db.session.add(location)
        db.session.commit()

    # associate foreign keys
    # map candidate offices to what their district prefix is (the space is important!)
    model_names = {
        'US House of Representatives': 'Texas Congressional ',
        'Texas House of Representatives': 'Texas House of Representatives ',
        'Texas Senate': 'Texas State Senate '
    }

    for c in all_candidates:
        candidate = Candidate.query.filter_by(name=c['name']).first()
        if c['running_office'] in model_names:
            location_name = model_names[c['running_office']] + c['location']
            location = Location.query.filter_by(name=location_name).first()
            location.candidates.append(candidate)
            db.session.commit()
        elif c['location'] == 'Texas':
            tx = Location.query.filter_by(name='Texas').first()
            tx.candidates.append(candidate)
            db.session.commit()

    for e in all_elections:
        election = Election.query.filter_by(name=e['election_name']).first()
        
        if e['office_name'] in model_names:
            location_name = model_names[e['office_name']] + e['location']
            location = Location.query.filter_by(name=location_name).first()
            location.elections.append(election)
            db.session.commit()
        elif e['location'] == 'Texas':
            tx = Location.query.filter_by(name='Texas').first()
            tx.elections.append(election)
            db.session.commit()

    # close files
    candidates_file.close()
    elections_file.close()
    tx_senate_file.close()
    tx_reps_file.close()
    us_reps_file.close()

def regen_images():
    all_c = Candidate.query.all()
    for c in all_c:
        if c.image in defaults:
            c.image = random.sample(defaults, 1)[0]
            db.session.commit()
    
    all_e = Election.query.all()
    for e in all_e:
        if e.image in defaults:
            e.image = random.sample(defaults, 1)[0]
            db.session.commit()
