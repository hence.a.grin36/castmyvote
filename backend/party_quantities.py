import json 

if __name__ == '__main__':
    with open('candidates.json', 'rb') as f:
        data = json.load(f)
   	party_num = dict()
   	for i in data:
   		name = i["name"]
   		party = i["party"]
   		if party in party_num:
   			party_num[party] = party_num[party] +1
   		else:
   			party_num[party] = 1
   	with open('partynum', 'wb') as f:
    		f.write(json.dumps(party_num))