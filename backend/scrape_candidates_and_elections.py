#!/usr/bin/env python3

import requests
from GoogleScraper import scrape_with_config, GoogleSearchError
from bs4 import BeautifulSoup
from threading import Thread
import logging
import json

def debug(thing):
    print(json.dumps(thing, indent=4))

def find_image(candidate):
    search_term = candidate['name'] + " Texas site:ballotpedia.org"
    search_link = f'https://www.bing.com/search?q={search_term}'
    search_request = requests.get(search_link)
    # print(f'searching for {candidate["name"]}, {search_link}')

    searchsoup = BeautifulSoup(search_request.content, 'html.parser')
    found = False
    first_res = searchsoup.find('li', attrs={'class':'b_algo'})
    if first_res and first_res.h2 and first_res.h2.a:
        href = first_res.h2.a['href']
        req = requests.get(href)
        imgsoup = BeautifulSoup(req.content, 'html.parser')
        if imgsoup:
            profilediv = imgsoup.find('div', attrs={'class': 'infobox person'})
            if profilediv:
                imgdiv = profilediv.find('img')
                if imgdiv and 'Silhouette_Placeholder_Image' not in imgdiv['src']:
                    found = True
                    print(f'[DEBUG] THREAD PROCESSING FOR {candidate["name"]} - in ballotpedia: {imgdiv["src"]}')
                    candidate['image'] = imgdiv['src']
    if not found:
        candidate['image'] = None

def parse():
    file = open('candidates.html', 'r')
    candidate_file = open('candidates.json', 'w')
    election_file = open('elections.json', 'w')
    all_candidates = []
    all_elections = []

    soup = BeautifulSoup(file, 'html.parser')

    all_content = soup.find('div', attrs={'ng-if': "SelectedDistrictType == 'All' || SelectedDistrictType == undefined"})
    blocks = [all_content.find('div', attrs={'class': 'ng-scope'})]
    curr = blocks[0]
    for i in range(4):
        next_sibling = curr.find_next_sibling('div', attrs={'class': 'ng-scope'})
        blocks.append(next_sibling)
        curr = next_sibling

    # classifications: TX (all of texas), US (us house districts), TX-H (tx house districts), TX-S (tx senate districts)
    # PART ONE: special case for first block on page because office/location is switched on the page
    print('[INFO] processing statewide block')
    parties = {'D': 'Democrat', 'R': 'Republican', 'L': 'Libertarian', 'I': 'Independent'}
    state_content = blocks[0].find('div', attrs={'class': 'content'})
    state_blocks = state_content.find_all('div', attrs={'class': 'outer_div'})
    for sb in state_blocks:
        subheading = sb.find('div', attrs={'class': 'left'}).a.find_all('div')
        office = ''
        for item in subheading:
            if item.contents:
                office += ' ' + item.contents[0]
        office = office[1:].replace(u"\u0003", '')

        place_i = office.find(' (Place')
        original = office
        if place_i != -1:
            # one of the judge positions
            office = office[:place_i]

        election_name = f'General - {office if place_i == -1 else original}'
        state_election = {'election_name': election_name, 'office_name': office, 'date': 'November 06, 2018',
            'type': 'General', 'classification': 'TX', 'location': 'Texas', 'upcoming': False, 'candidates': [], 'winner': None, 'image': None}

        rows = sb.find('div', attrs={'class': 'center_box'}).find_all('div', attrs={'class': 'row_race'})

        for r in rows:
            if 'one' not in r.attrs['class'] and 'two' not in r.attrs['class']:
                # only look at rows w/actual names
                hyperlink = r.find('span', attrs={'class': 'candidate_names'})
                name = hyperlink.a.contents[0]
                link = hyperlink.a['href']
                party = r.find('span', attrs={'class': 'circle'}).contents[0]
                is_incumbent = r.find('span', attrs={'class': 'incumbent'}) is not None
                candidate = {'name': name, 'party': parties[party], 'profile_link': link, 'incumbent': is_incumbent,
                    'location': 'Texas', 'running_office': office, 'elections': [election_name]}
                all_candidates.append(candidate)
                state_election['candidates'].append(name)

                if r.find('div', attrs={'class': 'winner_symbol'}):
                    state_election['winner'] = name
        all_elections.append(state_election)

    print('[INFO] processing other election blocks')
    # PART TWO: processing the rest of the election blocks by office (i.e.: all senate elections in a block)
    candidate_set = set((c['name'] for c in all_candidates))

    for i in range(1, len(blocks)):
        current_block = blocks[i]
        office = current_block.find('h4').contents[0].replace(u"\u0003", '')
        containers = current_block.find_all('div', attrs={'class': 'General ng-scope'})
        for container in containers:
            location = container.find_all('div', attrs={'class': 'district_heading'})
            for l in location:
                location = l.find('span').contents[0]
                classification = 'TX'
                seat = ''

                if 'Seat' not in location:
                    location = location
                else:
                    seat = location
                    classification = 'US'
                    location = 'Texas'

                header = container.find('div', attrs={'class': 'title center'}).find('a').contents[0]
                full_election_name = header.split('-')
                full_type = full_election_name[0].split()
                election_date = full_election_name[1][1:]
                election_type = ' '.join(full_type[:-1])
                upcoming = False
                election_name = f'{election_date} {election_type} - {office}'
                
                if 'Texas ' in office or 'US House' in office: # make election name like "tx house of rep (district 34)"
                    if 'US House' in office:
                        classification = 'US-H'
                    elif 'Senate' in office:
                        classification = 'TX-S'
                    else:
                        classification = 'TX-H'
                    election_name = f'{election_date} {election_type} - {office} ({location})'
                elif office == 'US Senate':
                    classification = 'US-S'
                    election_name = f'{election_date} {election_type} - {office} ({seat})'

                election = {'election_name': election_name, 'office_name': office, 'date': election_date, 'type': election_type,
                    'classification': classification, 'location': location, 'upcoming': upcoming, 'candidates': [], 'winner': None, 'image': None}

                rows = container.find('div', attrs={'class': 'center_box'}).find_all('div', attrs={'class': 'row_race'})

                for r in rows:
                    if 'one' not in r.attrs['class'] and 'two' not in r.attrs['class']:
                        # only look at rows w/actual names
                        hyperlink = r.find('span', attrs={'class': 'candidate_names'})
                        name = hyperlink.a.contents[0]
                        link = hyperlink.a['href']
                        party = r.find('span', attrs={'class': 'circle'}).contents[0]
                        is_incumbent = r.find('span', attrs={'class': 'incumbent'}) is not None
                        candidate = None
                        if name in candidate_set:
                            candidate = next(filter(lambda c: c['name'] == name, all_candidates))
                            candidate['elections'].append(election_name)
                        else:
                            candidate = {'name': name, 'party': parties[party], 'profile_link': link, 'incumbent': is_incumbent,
                                'location': location, 'running_office': office, 'elections': [election_name]}
                            candidate_set.add(name)
                            all_candidates.append(candidate)
                        election['candidates'].append(name)

                        if r.find('div', attrs={'class': 'winner_symbol'}):
                            election['winner'] = name
                all_elections.append(election)

    print('[INFO] processing candidate data')
    # PART THREE: process candidates
    threads = []
    for candidate in all_candidates:
        # print(f'processing for {candidate["name"]}')
        response = requests.get(candidate['profile_link'])
        soup = BeautifulSoup(response.content, 'html.parser')
        hiddentags = soup.find('input', attrs={'type': 'hidden'}).findNext('input').findNext('input')
        tag_string = hiddentags['value']
        dict_string = tag_string[1:len(tag_string) - 1]
        tag_dict = json.loads(dict_string)

        candidate['current_office'] = None
        candidate['contact'] = {}

        if tag_dict and 'CandidateMeta' in tag_dict and tag_dict['CandidateMeta'] and len(tag_dict['CandidateMeta']) > 0:
            meta = tag_dict['CandidateMeta'][0]
            current_office = meta['Office']
            
            if current_office:
                candidate['current_office'] = current_office

            for contact_type in ('Phone', 'Website', 'Twitter', 'LinkedIn', 'Facebook', 'EmailId'):
                if meta[contact_type]:
                    candidate['contact'][contact_type.lower()] = meta[contact_type]

            if not candidate['contact']:
                # candidate has no form of contact, must find website
                search_url = f'https://www.bing.com/search?q={candidate["name"]} website'
                request = requests.get(search_url)
                soup2 = BeautifulSoup(request.content, 'html.parser')
                if soup2.h2 and soup2.h2.a:
                    site = soup2.h2.a['href']
                    for platform in ('twitter', 'linkedin', 'facebook'):
                        # looking to see if social media found
                        if platform in site:
                            candidate['contact'][platform.capitalize()] = site

                    # social media not found, put down as site
                    if not candidate['contact']:
                        candidate['contact']['website'] = site

        # PART 3.5: look for profile photo on ballotpedia, if not found, turn to yahoo images
        candidate['image'] = None
        worker = Thread(target=find_image, args=(candidate,))
        threads.append(worker)
        print(f'[DEBUG] thread added - {candidate["name"]}')

    for t in threads:
        t.start()

    for t in threads:
        t.join()

    # PART 4: output
    print('[INFO] writing output files')
    election_file.write(json.dumps(all_elections, indent=4))
    candidate_file.write(json.dumps(all_candidates, indent=4))

    candidate_file.close()
    election_file.close()
    file.close()

if __name__ == '__main__':
    parse()