#!/usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import json

def convert_date(s):
    saved = s.split()
    if len(saved[1]) < 3:
        return saved[0] + " 0" + saved[1] + " " + saved[2]
    else:
        return s
#
# senate_file = open('senate_districts.json', "w")
# senate_districts = []
#
# for x in range(1, 32):
#     page_link = "https://ballotpedia.org/Texas_State_Senate_District_" + str(x)
#     page_request = requests.get(page_link)
#     soup = BeautifulSoup(page_request.content, 'lxml')
#     curr_dist = {}
#     curr_dist["name"] = "Texas State Senate District " + str(x)
#
#     if soup:
#         items = soup.find("table", attrs={'class': "infobox"})
#         if items:
#             elements = items.find_all("tr")
#             for e in elements:
#                 individual_tokens = e.find_all("td")
#                 if len(individual_tokens) >= 2:
#                     if len(individual_tokens[0].text) > 0:
#                         curr_tok = individual_tokens[0].text.lower().replace(" ", "_")
#                         if  curr_tok == "race":
#                             race_line = individual_tokens[1].text[:-3]
#                             race_arr = race_line.split(", ")
#                             dc = {}
#                             for crap in race_arr:
#                                 temp = crap.split()
#                                 dc[temp[1].lower()] = temp[0]
#                             curr_dist[curr_tok] = dc
#                         elif curr_tok == "ethnicity":
#                             ethn_line = individual_tokens[1].text
#                             eth_arr = ethn_line.split(", ")
#                             dc = {}
#                             for crap2 in eth_arr:
#                                 temp2 = crap2.split()
#                                 if len(temp2) > 2:
#                                     dc["not_hispanic"] = temp2[0]
#                                 else:
#                                     dc[temp2[1].lower()] = temp2[0]
#                             curr_dist[curr_tok] = dc
#                         elif curr_tok == "current_incumbent":
#                             curr_dist[curr_tok] = individual_tokens[1].text[:-1]
#                         elif curr_tok == "next_general_election":
#                             curr_dist[curr_tok] = convert_date(individual_tokens[1].text)
#                         else:
#                             curr_dist[curr_tok] = individual_tokens[1].text
#     senate_districts.append(curr_dist)
#
# senate_file.write(json.dumps(senate_districts, indent=4))

rep_file = open('rep_districts.json', "w")
rep_districts = []

for x in range(1, 151):
    page_link = "https://ballotpedia.org/Texas_House_of_Representatives_District_" + str(x)
    page_request = requests.get(page_link)
    soup = BeautifulSoup(page_request.content, 'lxml')
    curr_dist = {}
    curr_dist["name"] = "Texas House of Representatives District " + str(x)

    if soup:
        items = soup.find("table", attrs={'class': "infobox"})
        if items:
            elements = items.find_all("tr")
            for e in elements:
                individual_tokens = e.find_all("td")
                if len(individual_tokens) >= 2:
                    if len(individual_tokens[0].text) > 0:
                        curr_tok = individual_tokens[0].text.lower().replace(" ", "_")
                        if  curr_tok == "race":
                            race_line = individual_tokens[1].text[:-3]
                            race_arr = race_line.split(", ")
                            dc = {}
                            for crap in race_arr:
                                temp = crap.split()
                                dc[temp[1].lower()] = temp[0]
                            curr_dist[curr_tok] = dc
                        elif curr_tok == "ethnicity":
                            ethn_line = individual_tokens[1].text
                            eth_arr = ethn_line.split(", ")
                            dc = {}
                            for crap2 in eth_arr:
                                temp2 = crap2.split()
                                if len(temp2) > 2:
                                    dc["not_hispanic"] = temp2[0]
                                else:
                                    dc[temp2[1].lower()] = temp2[0]
                            curr_dist[curr_tok] = dc
                        elif curr_tok == "current_incumbent":
                            curr_dist[curr_tok] = individual_tokens[1].text[:-1]
                        elif curr_tok == "next_general_election":
                            curr_dist[curr_tok] = convert_date(individual_tokens[1].text)
                        else:
                            curr_dist[curr_tok] = individual_tokens[1].text
    rep_districts.append(curr_dist)

rep_file.write(json.dumps(rep_districts, indent=4))
#
# us_rep_file = open('us_rep_districts.json', "w")
# us_rep_districts = []
# ordinal = lambda n: "%d%s" % (n,"tsnrhtdd"[(n//10%10!=1)*(n%10<4)*n%10::4])
#
# for n in range(1,37):
#     page_link = "https://ballotpedia.org/Texas%27_" + str(ordinal(n)) + "_Congressional_District"
#     page_request = requests.get(page_link)
#     soup = BeautifulSoup(page_request.content, 'lxml')
#     curr_dist = {}
#     curr_dist["name"] = "Texas Congressional District " + str(n)
#     if soup:
#         items = soup.find("table", attrs={'class': "infobox"})
#         if items:
#             elements = items.find_all("tr")
#             counter = 0
#             for e in elements:
#                 lst = e.text
#                 if counter == 2:
#                     line = lst[18:]
#                     toks = line.split()
#                     curr_dist["current_incumbent"] = toks[0] + " " + toks[1]
#                 elif counter > 3 and counter <= 6:
#                     line = lst
#                     toks = line.split(": ")
#                     if toks[0].lower().replace("\n", "") == "gender":
#                         dc = {}
#                         gender_line = toks[1].replace("\n", "")
#                         gender_arr = gender_line.split(", ")
#                         for crap1 in gender_arr:
#                             temp1 = crap1.split()
#                             dc[temp1[1].lower()] = temp1[0]
#                         curr_dist["gender"] = dc
#                     elif toks[0].lower() == "\nrace[1]":
#                         dc = {}
#                         race_line = toks[1].replace("\n", "")
#                         race_arr = race_line.split(", ")
#                         for crap in race_arr:
#                             temp = crap.split()
#                             dc[temp[1].lower()] = temp[0]
#                         curr_dist["race"] = dc
#                     else:
#                         curr_dist[toks[0].lower().replace("\n", "")] = toks[1].replace("\n", "")
#                 counter = counter+1
#     us_rep_districts.append(curr_dist)
#
# us_rep_file.write(json.dumps(us_rep_districts, indent=4))
