# pylint: disable=no-member

import unittest
import flask_testing
import os
from flask import Flask
from models import Election, Candidate, Location, db, regen_images, defaults

class TestCases(flask_testing.TestCase):
    # Test Author: Jessie Chen
    SQLALCHEMY_DATABASE_URI = "sqlite://"
    SQLALCHEMY_BINDS = None
    TESTING = True

    def create_app(self):
        app = Flask(__name__)
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        app.config['SQLALCHEMY_BINDS'] = None
        app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
        db.init_app(app)
        return app

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()

    def test_model_candidate_db(self):
        candidate = Candidate(name="Greg Abbott",
                              party="Republican",
                              incumbent=True,
                              running_office="Governor",
                              current_office="Governor",
                              image="https://api.ballotpedia.org/v3/thumbnail/200/300/crop/best/GregAbbott2015.jpg",
                              contact_info={"phone": "(512) 463 - 2000"})
        db.session.add(candidate)
        db.session.commit()
        returned_obj = Candidate.query.filter_by(name='Greg Abbott').first()
        returned_dict = returned_obj.as_dict()
        expected_dict = {
            'name': "Greg Abbott",
            'party': "Republican",
            'incumbent': True,
            'running_office': "Governor",
            'current_office': "Governor",
            'image': "https://api.ballotpedia.org/v3/thumbnail/200/300/crop/best/GregAbbott2015.jpg",
            'contact_info': {"Phone": "(512) 463 - 2000"}
        }
        self.assertEqual(expected_dict["name"], returned_dict["name"])
        self.assertEqual(expected_dict["party"], returned_dict["party"])
        self.assertEqual(expected_dict["incumbent"], returned_dict["incumbent"])
        self.assertEqual(expected_dict["running_office"], returned_dict["running_office"])

    def test_model_election_db(self):
        election = Election(name="General Midterm Election - Governor",
                            office_name="Governor",
                            date="November 6th, 2018",
                            election_type="General",
                            upcoming=True,
                            classification="TX",
                            winner=None,
                            image='fake.jpg')
        db.session.add(election)
        db.session.commit()
        returned_obj = Election.query.filter_by(name='General Midterm Election - Governor').first()
        returned_dict = returned_obj.as_dict()
        expected_dict = {
            'name': "General Midterm Election - Governor",
            'office_name': "Governor",
            'date': "November 6th, 2018",
            'election_type': "General",
            'upcoming': True,
        }
        self.assertEqual(expected_dict["name"], returned_dict["name"])
        self.assertEqual(expected_dict["office_name"], returned_dict["office_name"])
        self.assertEqual(expected_dict["date"], returned_dict["date"])
        self.assertEqual(expected_dict["election_type"], returned_dict["election_type"])
        self.assertEqual(expected_dict["upcoming"], returned_dict["upcoming"])

    def test_model_location_db(self):
        location = Location({'name': "Texas",
                            'current_incumbent':"Greg Abbott",
                            'population':28704330,
                            'voting_age':"71.8% age 18 and over"})
        db.session.add(location)
        db.session.commit()
        returned_obj = Location.query.filter_by(name='Texas').first()
        returned_dict = returned_obj.as_dict()
        expected_dict = {
            "name": "Texas",
            "current_incumbent": "Greg Abbott",
            "population": 28704330,
            "gender": {
                "female": "50.4%",
                "male": "49.6%"
            },
            "race": {
                "white": "74.8%",
                "black": "11.9%",
                "asian": "4.4%",
                "other": "8.8%"
            },
            "ethnicity": {
                "hispanic": "37%",
                "not_hispanic": "63%"
            },
            "voting_age": "71.8% age 18 and over"
        }

        self.assertEqual(expected_dict["name"], returned_dict["name"])
        self.assertEqual(expected_dict["current_incumbent"], returned_dict["current_incumbent"])
        self.assertEqual(expected_dict["population"], returned_dict["population"])
        self.assertEqual(expected_dict["voting_age"], returned_dict["voting_age"])

    def test_name_sorting(self):
        location1 = Location({'name': "Z",
                            'current_incumbent':"Greg Abbott",
                            'population':28704330,
                            'voting_age':"71.8% age 18 and over"})
        location2 = Location({'name': "C",
                            'current_incumbent':"Greg Kabbott",
                            'population':28704330,
                            'voting_age':"71.8% age 18 and over"})
        location3 = Location({'name': "Ce",
                            'current_incumbent':"Greg Zabbott",
                            'population':28704330,
                            'voting_age':"71.8% age 18 and over"})
        db.session.add(location1)
        db.session.add(location2)
        db.session.add(location3)
        db.session.commit()
        sort_by = "name"
        returned_obj = Location.query.order_by(getattr(Location, sort_by))
        self.assertEqual(returned_obj[0], location2)
        self.assertEqual(returned_obj[1], location3)
        self.assertEqual(returned_obj[2], location1)

    def test_name_population(self):
        location1 = Location({'name': "Z",
                            'current_incumbent':"Greg Abbott",
                            'population':28704330,
                            'voting_age':"71.8% age 18 and over"})
        location2 = Location({'name': "C",
                            'current_incumbent':"Greg Kabbott",
                            'population':28704329,
                            'voting_age':"71.8% age 18 and over"})
        location3 = Location({'name': "Ce",
                            'current_incumbent':"Greg Zabbott",
                            'population':28704331,
                            'voting_age':"71.8% age 18 and over"})
        db.session.add(location1)
        db.session.add(location2)
        db.session.add(location3)
        db.session.commit()
        sort_by = "population"
        returned_obj = Location.query.order_by(getattr(Location, sort_by))
        self.assertEqual(returned_obj[0], location2)
        self.assertEqual(returned_obj[1], location1)
        self.assertEqual(returned_obj[2], location3)

    def test_as_dict(self):
        candidate = Candidate(name="Greg Abbott",
                              party="Republican",
                              incumbent=True,
                              running_office="Governor",
                              current_office="Governor",
                              image="https://api.ballotpedia.org/v3/thumbnail/200/300/crop/best/GregAbbott2015.jpg",
                              contact_info={"phone": "(512) 463 - 2000"})
        db.session.add(candidate)
        db.session.commit()
        expected = {
            'id': 1,
            'name': 'Greg Abbott',
            'party': 'Republican',
            'incumbent': True,
            'running_office': 'Governor',
            'current_office': 'Governor',
            'image': 'https://api.ballotpedia.org/v3/thumbnail/200/300/crop/best/GregAbbott2015.jpg',
            'contact_info': {
                'phone': '(512) 463 - 2000'
            }
        }
        candidate = Candidate.query.filter_by(name='Greg Abbott').first()
        self.assertEqual(candidate.as_dict(), expected)
    
    def test_as_dict_linked(self):
        candidate = Candidate(name="Greg Abbott",
                              party="Republican",
                              incumbent=True,
                              running_office="Governor",
                              current_office="Governor",
                              image="https://api.ballotpedia.org/v3/thumbnail/200/300/crop/best/GregAbbott2015.jpg",
                              contact_info={"phone": "(512) 463 - 2000"})
        db.session.add(candidate)
        db.session.commit()
        location = Location({'name': "Texas",
                            'current_incumbent':"Greg Abbott",
                            'population':28704330,
                            'voting_age':"71.8% age 18 and over"})
        candidate = Candidate.query.filter_by(name='Greg Abbott').first()
        location.candidates.append(candidate)
        db.session.add(location)
        db.session.commit()
        expected = {
            'id': 1,
            'name': 'Greg Abbott',
            'party': 'Republican',
            'incumbent': True,
            'running_office': 'Governor',
            'current_office': 'Governor',
            'image': 'https://api.ballotpedia.org/v3/thumbnail/200/300/crop/best/GregAbbott2015.jpg',
            'contact_info': {
                'phone': '(512) 463 - 2000'
            },
            'location': {
                'id': 1,
                'name': 'Texas',
                'current_incumbent': 'Greg Abbott',
                'population': 28704330,
                'race': None,
                'ethnicity': None,
                'voting_age': '71.8% age 18 and over',
                'gender': None
            }
        }
        self.assertEqual(candidate.as_dict(link_locations=True), expected)

    def test_regen_images(self):
        candidate = Candidate(name="fake candidate",
                              party="Republican",
                              incumbent=True,
                              running_office="fake office",
                              current_office="also fake office",
                              image="https://www.brookings.edu/wp-content/uploads/2016/06/congress006-1.jpg",
                              contact_info={"phone": "(512) 463 - 2000"})
        db.session.add(candidate)
        db.session.commit()
        regen_images()
        candidate = Candidate.query.filter_by(name='fake candidate').first()
        self.assertTrue(candidate.image in defaults)

    def test_filtering(self):
        candidate = Candidate(name="fake candidate",
                              party="Republican",
                              incumbent=True,
                              running_office="fake office",
                              current_office="also fake office",
                              image="https://www.brookings.edu/wp-content/uploads/2016/06/congress006-1.jpg",
                              contact_info={"phone": "(512) 463 - 2000"})
        candidate2 = Candidate(name="fake candidate2",
                              party="Republican",
                              incumbent=False,
                              running_office="fake office",
                              current_office="also fake office",
                              image="fake URL",
                              contact_info={"phone": "(302) 222 - 1111"})
        db.session.add(candidate)
        db.session.add(candidate2)
        db.session.commit()
        expected = Candidate(name="fake candidate",
                              party="Republican",
                              incumbent=True,
                              running_office="fake office",
                              current_office="also fake office",
                              image="https://www.brookings.edu/wp-content/uploads/2016/06/congress006-1.jpg",
                              contact_info={"phone": "(512) 463 - 2000"})
        filtered = Candidate.query.filter_by(incumbent=True)
        self.assertEqual(filtered[0], candidate)
        self.assertTrue(filtered.count() == 1)

if __name__ == "__main__":
    unittest.main()
